﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Pipelines;

namespace Fluorite.Strainer.UnitTests.Services.Pipelines;

public class StrainerPipelineBuilderFactoryTests
{
    [Fact]
    public void Should_Create_PipelineContextBuilder()
    {
        // Arrange
        var options = new StrainerOptions();
        var optionsProvider = Substitute.For<IStrainerOptionsProvider>();
        optionsProvider.GetStrainerOptions().Returns(options);
        var filterPipelineOperation = Substitute.For<IFilterPipelineOperation>();
        var sortPipelineOperation = Substitute.For<ISortPipelineOperation>();
        var paginatePipelineOperation = Substitute.For<IPaginatePipelineOperation>();

        // Act
        var factory = new StrainerPipelineBuilderFactory(
            filterPipelineOperation,
            sortPipelineOperation,
            paginatePipelineOperation,
            optionsProvider);
        var result = factory.CreateBuilder();

        // Assert
        result.Should().NotBeNull();
    }
}
