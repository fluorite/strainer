﻿using Fluorite.Strainer.Services.Pipelines;

namespace Fluorite.Strainer.UnitTests.Services.Pipelines;

public class PipelineContextTests
{
    [Fact]
    public void Should_Create_PipelineContext()
    {
        // Arrange
        var pipelineBuilderFactory = Substitute.For<IStrainerPipelineBuilderFactory>();

        // Act
        var result = new PipelineContext(pipelineBuilderFactory);

        // Assert
        result.BuilderFactory.Should().BeSameAs(pipelineBuilderFactory);
    }
}
