﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Pipelines;
using Fluorite.Strainer.Services.Sorting;

namespace Fluorite.Strainer.UnitTests.Services;

public class StrainerContextTests
{
    [Fact]
    public void Should_Create_StrainerContext()
    {
        // Arrange
        var customMethodsConfigurationProvider = Substitute.For<IConfigurationCustomMethodsProvider>();
        var options = new StrainerOptions();
        var optionsProvider = Substitute.For<IStrainerOptionsProvider>();
        optionsProvider.GetStrainerOptions().Returns(options);
        var filterContext = Substitute.For<IFilterContext>();
        var sortingContext = Substitute.For<ISortingContext>();
        var metadataFacade = Substitute.For<IMetadataFacade>();
        var pipelineContext = Substitute.For<IPipelineContext>();

        // Act
        var result = new StrainerContext(
            customMethodsConfigurationProvider,
            optionsProvider,
            filterContext,
            sortingContext,
            metadataFacade,
            pipelineContext);

        // Assert
        result.CustomMethods.Should().BeSameAs(customMethodsConfigurationProvider);
        result.Filter.Should().BeSameAs(filterContext);
        result.Metadata.Should().BeSameAs(metadataFacade);
        result.Options.Should().BeSameAs(options);
        result.Pipeline.Should().BeSameAs(pipelineContext);
        result.Sorting.Should().BeSameAs(sortingContext);
    }
}
