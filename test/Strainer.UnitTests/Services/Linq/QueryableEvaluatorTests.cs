﻿using Fluorite.Strainer.Services.Linq;
using System.Collections;
using System.Collections.ObjectModel;

namespace Fluorite.Strainer.UnitTests.Services.Linq;

public class QueryableEvaluatorTests
{
    private readonly QueryableEvaluator _evaluator = new();

    [Fact]
    public void Should_Return_QueryableEvaluation_ForUnknownQueryable()
    {
        // Arrange
        var queryable = Substitute.For<IQueryable<string>>();

        // Act
        var result = _evaluator.IsMaterialized(queryable);

        // Assert
        result.Should().BeFalse();
    }

    [Theory]
    [ClassData(typeof(CollectionSourceData))]
    public void Should_Return_QueryableEvaluation_ForKnownCollectionTypes(IQueryable<string> queryable)
    {
        // Act
        var result = _evaluator.IsMaterialized(queryable);

        // Assert
        result.Should().BeTrue();
    }

    public class CollectionSourceData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            return new object[]
            {
                Enumerable.Empty<string>().AsQueryable(),
                new List<string>().AsQueryable(),
                Array.Empty<string>().AsQueryable(),
                new ReadOnlyCollection<string>(Array.Empty<string>()).AsQueryable(),
            }
            .Chunk(1)
            .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
