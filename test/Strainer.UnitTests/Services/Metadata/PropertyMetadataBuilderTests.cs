﻿using Fluorite.Strainer.Exceptions;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Services.Metadata;

namespace Fluorite.Strainer.UnitTests.Services.Metadata;

public class PropertyMetadataBuilderTests
{
    [Fact]
    public void Should_Save_PropertyMetadata_WhenCreatingBuilder()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);

        // Act
        _ = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(propertyName);
        propertyMetadata[type][propertyName].DisplayName.Should().BeNull();
        propertyMetadata[type][propertyName].IsDefaultSorting.Should().BeFalse();
        propertyMetadata[type][propertyName].DefaultSortingWay.Should().BeNull();
        propertyMetadata[type][propertyName].IsFilterable.Should().BeFalse();
        propertyMetadata[type][propertyName].IsSortable.Should().BeFalse();
        propertyMetadata[type][propertyName].Name.Should().Be(propertyName);
        propertyMetadata[type][propertyName].PropertyInfo.Should().BeSameAs(propertyInfo);
    }

    [Fact]
    public void Should_Save_PropertyMetadata_WhenMarkingAsFilterable()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);

        // Act
        var builder = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName);
        builder.IsFilterable();

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(propertyName);
        propertyMetadata[type][propertyName].DisplayName.Should().BeNull();
        propertyMetadata[type][propertyName].IsDefaultSorting.Should().BeFalse();
        propertyMetadata[type][propertyName].DefaultSortingWay.Should().BeNull();
        propertyMetadata[type][propertyName].IsFilterable.Should().BeTrue();
        propertyMetadata[type][propertyName].IsSortable.Should().BeFalse();
        propertyMetadata[type][propertyName].Name.Should().Be(propertyName);
        propertyMetadata[type][propertyName].PropertyInfo.Should().BeSameAs(propertyInfo);
    }

    [Fact]
    public void Should_Save_PropertyMetadata_WhenMarkingAsSortable()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);

        // Act
        var builder = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName);
        var sortableBuilder = builder.IsSortable();

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(propertyName);
        propertyMetadata[type][propertyName].DisplayName.Should().BeNull();
        propertyMetadata[type][propertyName].IsDefaultSorting.Should().BeFalse();
        propertyMetadata[type][propertyName].DefaultSortingWay.Should().BeNull();
        propertyMetadata[type][propertyName].IsFilterable.Should().BeFalse();
        propertyMetadata[type][propertyName].IsSortable.Should().BeTrue();
        propertyMetadata[type][propertyName].Name.Should().Be(propertyName);
        propertyMetadata[type][propertyName].PropertyInfo.Should().BeSameAs(propertyInfo);

        sortableBuilder.Should().NotBeNull();
    }

    [Fact]
    public void Should_Save_PropertyMetadata_WhenSettingDisplayName()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);
        var displayName = "SuperTitle";

        // Act
        var builder = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName);
        builder.HasDisplayName(displayName);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(displayName);
        propertyMetadata[type][displayName].DisplayName.Should().Be(displayName);
        propertyMetadata[type][displayName].IsDefaultSorting.Should().BeFalse();
        propertyMetadata[type][displayName].DefaultSortingWay.Should().BeNull();
        propertyMetadata[type][displayName].IsFilterable.Should().BeFalse();
        propertyMetadata[type][displayName].IsSortable.Should().BeFalse();
        propertyMetadata[type][displayName].Name.Should().Be(propertyName);
        propertyMetadata[type][displayName].PropertyInfo.Should().BeSameAs(propertyInfo);
    }

    [Fact]
    public void Should_Save_PropertyMetadata_WhenSettingDisplayNameMultipletimes()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);
        var displayName1 = "SuperTitle";
        var displayName2 = "SuperTitle";
        var displayName3 = "SuperTitle";

        // Act
        var builder = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName);
        builder.HasDisplayName(displayName1);
        builder.HasDisplayName(displayName2);
        builder.HasDisplayName(displayName3);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(displayName3);
        propertyMetadata[type][displayName3].DisplayName.Should().Be(displayName3);
        propertyMetadata[type][displayName3].IsDefaultSorting.Should().BeFalse();
        propertyMetadata[type][displayName3].DefaultSortingWay.Should().BeNull();
        propertyMetadata[type][displayName3].IsFilterable.Should().BeFalse();
        propertyMetadata[type][displayName3].IsSortable.Should().BeFalse();
        propertyMetadata[type][displayName3].Name.Should().Be(propertyName);
        propertyMetadata[type][displayName3].PropertyInfo.Should().BeSameAs(propertyInfo);
    }

    [Fact]
    public void Should_Allow_ForOverwritingOldName_UsingDisplayName()
    {
        // Arrange
        var type = typeof(Blog);
        var propertyName = nameof(Blog.Title);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var displayName = "SuperTitle";
        var preexistingMetadata = Substitute.For<IPropertyMetadata>();
        preexistingMetadata.PropertyInfo.Returns(propertyInfo);
        preexistingMetadata.DisplayName.ReturnsNull();
        preexistingMetadata.Name.Returns(propertyName);
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>
        {
            [type] = new Dictionary<string, IPropertyMetadata>
            {
                [propertyName] = preexistingMetadata,
            },
        };
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();

        // Act
        var builder = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName);
        builder.HasDisplayName(displayName);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(displayName);
        propertyMetadata[type][displayName].DisplayName.Should().Be(displayName);
    }

    [Fact]
    public void Should_Throw_OnAttemptOfOverwriting_DifferentPropertyWithAlreadyUsedName()
    {
        // Arrange
        var type = typeof(Blog);
        var authorPropertyName = nameof(Blog.Author);
        var titlePropertyName = nameof(Blog.Title);
        var authorPropertyInfo = type.GetProperty(nameof(Blog.Author));
        var titlePropertyInfo = type.GetProperty(nameof(Blog.Title));
        var preexistingMetadata = Substitute.For<IPropertyMetadata>();
        preexistingMetadata.PropertyInfo.Returns(titlePropertyInfo);
        preexistingMetadata.DisplayName.ReturnsNull();
        preexistingMetadata.Name.Returns(titlePropertyName);
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>
        {
            [type] = new Dictionary<string, IPropertyMetadata>
            {
                [titlePropertyName] = preexistingMetadata,
            },
        };
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();

        // Act
        var builder = new PropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            authorPropertyInfo,
            authorPropertyName);
        Action act = () => builder.HasDisplayName(titlePropertyName);

        // Assert
        act.Should().ThrowExactly<StrainerException>()
            .WithMessage(
                $"Cannot overwrite different property {titlePropertyName} " +
                $"on type {type.Name} with metadata using display name {titlePropertyName} for property {authorPropertyName}.");
    }

    private class Blog
    {
        public string Title { get; set; }

        public string Author { get; set; }
    }
}
