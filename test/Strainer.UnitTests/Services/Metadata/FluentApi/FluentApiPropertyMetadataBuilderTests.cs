﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Metadata.FluentApi;
using System.Reflection;

namespace Fluorite.Strainer.UnitTests.Services.Metadata.FluentApi;

public class FluentApiPropertyMetadataBuilderTests
{
    private readonly IStrainerOptionsProvider _strainerOptionsProviderMock = Substitute.For<IStrainerOptionsProvider>();

    private readonly FluentApiPropertyMetadataBuilder _builder;

    public FluentApiPropertyMetadataBuilderTests()
    {
        _builder = new FluentApiPropertyMetadataBuilder(_strainerOptionsProviderMock);
    }

    [Fact]
    public void Should_Return_DefaultMetadata()
    {
        // Arrange
        var name = "foo";
        var propertyInfo = Substitute.For<PropertyInfo>();
        var objectMetadata = Substitute.For<IObjectMetadata>();
        objectMetadata.DefaultSortingPropertyName.Returns(name);
        objectMetadata.DefaultSortingPropertyInfo.Returns(propertyInfo);

        // Act
        var result = _builder.BuildDefaultMetadata(objectMetadata);

        // Assert
        result.Should().NotBeNull();
        result.IsDefaultSorting.Should().BeTrue();
        result.Should().BeEquivalentTo(
            objectMetadata,
            options => options
                .WithMapping<IPropertyMetadata>(e => e.DefaultSortingPropertyInfo, s => s.PropertyInfo)
                .WithMapping<IPropertyMetadata>(e => e.DefaultSortingPropertyName, s => s.Name));
    }

    [Theory]
    [InlineData(SortingWay.Ascending)]
    [InlineData(SortingWay.Descending)]
    public void Should_Return_DefaultMetadataForProperty_UsingTheSamePropertyInfoAsDefaultProperty(SortingWay defaultSortingWay)
    {
        // Arrange
        var name = "foo";
        var propertyInfo = Substitute.For<PropertyInfo>();
        propertyInfo.Name.Returns(name);
        var objectMetadata = Substitute.For<IObjectMetadata>();
        objectMetadata.DefaultSortingWay.Returns(defaultSortingWay);
        objectMetadata.DefaultSortingPropertyName.Returns(name);
        objectMetadata.DefaultSortingPropertyInfo.Returns(propertyInfo);

        // Act
        var result = _builder.BuildMetadataForProperty(objectMetadata, propertyInfo);

        // Assert
        result.Should().NotBeNull();
        result.IsDefaultSorting.Should().BeTrue();
        result.Should().BeEquivalentTo(
            objectMetadata,
            options => options
                .WithMapping<IPropertyMetadata>(e => e.DefaultSortingPropertyInfo, s => s.PropertyInfo)
                .WithMapping<IPropertyMetadata>(e => e.DefaultSortingPropertyName, s => s.Name));
    }

    [Theory]
    [InlineData(SortingWay.Ascending)]
    [InlineData(SortingWay.Descending)]
    public void Should_Return_DefaultMetadataForProperty_UsingDifferentPropertyInfoAsDefaultProperty(SortingWay defaultSortingWay)
    {
        // Arrange
        var name = "foo";
        var propertyInfo = Substitute.For<PropertyInfo>();
        var differentPropertyInfo = Substitute.For<PropertyInfo>();
        differentPropertyInfo.Name.Returns(name);
        var objectMetadata = Substitute.For<IObjectMetadata>();
        objectMetadata.DefaultSortingPropertyName.Returns(name);
        objectMetadata.DefaultSortingPropertyInfo.Returns(propertyInfo);

        var strainerOptions = new StrainerOptions
        {
            DefaultSortingWay = defaultSortingWay,
        };

        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);

        // Act
        var result = _builder.BuildMetadataForProperty(objectMetadata, differentPropertyInfo);

        // Assert
        result.Should().NotBeNull();
        result.IsDefaultSorting.Should().BeFalse();
        result.PropertyInfo.Should().BeSameAs(differentPropertyInfo);
        result.DefaultSortingWay.Should().Be(strainerOptions.DefaultSortingWay);
        result.Should().BeEquivalentTo(
            objectMetadata,
            options => options
                .Excluding(e => e.DefaultSortingWay)
                .Excluding(e => e.DefaultSortingPropertyInfo)
                .WithMapping<IPropertyMetadata>(e => e.DefaultSortingPropertyName, s => s.Name));
    }
}
