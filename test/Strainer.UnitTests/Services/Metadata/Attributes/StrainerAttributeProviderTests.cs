﻿using Fluorite.Strainer.Attributes;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Metadata.Attributes;
using System.Reflection;

namespace Fluorite.Strainer.UnitTests.Services.Metadata.Attributes;

public class StrainerAttributeProviderTests
{
    private readonly IPropertyInfoProvider _propertyInfoProviderMock = Substitute.For<IPropertyInfoProvider>();

    private readonly StrainerAttributeProvider _provider;

    public StrainerAttributeProviderTests()
    {
        _provider = new StrainerAttributeProvider(_propertyInfoProviderMock);
    }

    public interface ITypeFormatter
    {
        string GetPrettyTypeName(Type type);
    }

    [Fact]
    public void Should_Throw_WhenTypeIsNull()
    {
        // Act
        Action act = () => _provider.GetObjectAttribute(type: null);

        // Assert
        act.Should().ThrowExactly<ArgumentNullException>();
    }

    [Fact]
    public void Should_Return_NullWhen_ObjectAttributeIsNotFound()
    {
        // Arrange
        var type = typeof(Version);

        // Act
        var result = _provider.GetObjectAttribute(type);

        // Assert
        result.Should().BeNull();
    }

    [Fact]
    public void Should_Return_ObjectAttribute_WhenFound()
    {
        // Arrange
        var objectAttribute = new StrainerObjectAttribute("foo");
        var typeMock = Substitute.For<Type>();
        var defaultSortingPropertyInfo = Substitute.For<PropertyInfo>();
        typeMock
            .GetCustomAttributes(typeof(StrainerObjectAttribute), false)
            .Returns(new[] { objectAttribute });
        _propertyInfoProviderMock
            .GetPropertyInfo(Arg.Is<Type>(x => ReferenceEquals(x, typeMock)), objectAttribute.DefaultSortingPropertyName)
            .Returns(defaultSortingPropertyInfo);

        // Act
        var result = _provider.GetObjectAttribute(typeMock);

        // Assert
        result.Should().NotBeNull();
        result.Should().BeSameAs(objectAttribute);
        result.DefaultSortingPropertyInfo.Should().NotBeNull();
        result.DefaultSortingPropertyInfo.Should().BeSameAs(defaultSortingPropertyInfo);
    }

    [Fact]
    public void Should_Throw_WhenPropertyInfoIsNull()
    {
        // Act
        Action act = () => _provider.GetPropertyAttribute(propertyInfo: null);

        // Assert
        act.Should().ThrowExactly<ArgumentNullException>();
    }

    [Fact]
    public void Should_Return_PropertyAttribute_WhenFound()
    {
        // Arrange
        var propertyAttribute = new StrainerPropertyAttribute();
        var propertyInfoMock = Substitute.For<PropertyInfo>();
        propertyInfoMock
            .GetCustomAttributes(Arg.Is(typeof(StrainerPropertyAttribute)), Arg.Is(false))
            .Returns(new[] { propertyAttribute });

        // Act
        var result = _provider.GetPropertyAttribute(propertyInfoMock);

        // Assert
        result.Should().NotBeNull();
        result.Should().BeSameAs(propertyAttribute);
        result.PropertyInfo.Should().BeSameAs(propertyInfoMock);
    }

    [Fact]
    public void Should_Return_NullWhen_PropertyAttributeNotFound()
    {
        // Arrange
        var propertyInfoMock = Substitute.For<PropertyInfo>();
        propertyInfoMock
            .GetCustomAttributes(Arg.Is(typeof(StrainerPropertyAttribute)), Arg.Is(false))
            .ReturnsNull();

        // Act
        var result = _provider.GetPropertyAttribute(propertyInfoMock);

        // Assert
        result.Should().BeNull();
    }
}
