﻿using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Services.Sorting;

namespace Fluorite.Strainer.UnitTests.Services.Metadata;

public class SortPropertyMetadataBuilderTests
{
    [Fact]
    public void Should_Save_PropertyMetadata_WhenCreatingBuilder()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);
        var basePropertyMetadata = Substitute.For<IPropertyMetadata>();
        basePropertyMetadata.DisplayName.ReturnsNull();

        // Act
        _ = new SortPropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName,
            basePropertyMetadata);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(propertyName);
        propertyMetadata[type][propertyName].DisplayName.Should().BeNull();
        propertyMetadata[type][propertyName].IsDefaultSorting.Should().BeFalse();
        propertyMetadata[type][propertyName].DefaultSortingWay.Should().BeNull();
        propertyMetadata[type][propertyName].IsFilterable.Should().BeFalse();
        propertyMetadata[type][propertyName].IsSortable.Should().BeFalse();
        propertyMetadata[type][propertyName].Name.Should().Be(propertyName);
        propertyMetadata[type][propertyName].PropertyInfo.Should().BeSameAs(propertyInfo);
        defaultMetadata.Should().BeEmpty();
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void Should_Save_PropertyMetadata_WithDefaultSortingOption(bool isDescending)
    {
        // Arrange
        var sortingWay = isDescending ? SortingWay.Descending : SortingWay.Ascending;
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var type = typeof(Blog);
        var propertyInfo = type.GetProperty(nameof(Blog.Title));
        var propertyName = nameof(Blog.Title);
        var basePropertyMetadata = Substitute.For<IPropertyMetadata>();
        basePropertyMetadata.DisplayName.ReturnsNull();

        // Act
        var builder = new SortPropertyMetadataBuilder<Blog>(
            propertyMetadata,
            defaultMetadata,
            propertyInfo,
            propertyName,
            basePropertyMetadata);
        builder.IsDefaultSort(isDescending);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Keys.Should().BeEquivalentTo([type]);
        propertyMetadata[type].Should().NotBeNullOrEmpty();
        propertyMetadata[type].Keys.Should().BeEquivalentTo(propertyName);
        propertyMetadata[type][propertyName].DisplayName.Should().BeNull();
        propertyMetadata[type][propertyName].IsDefaultSorting.Should().BeTrue();
        propertyMetadata[type][propertyName].DefaultSortingWay.Should().Be(sortingWay);
        propertyMetadata[type][propertyName].IsFilterable.Should().BeFalse();
        propertyMetadata[type][propertyName].IsSortable.Should().BeFalse();
        propertyMetadata[type][propertyName].Name.Should().Be(propertyName);
        propertyMetadata[type][propertyName].PropertyInfo.Should().BeSameAs(propertyInfo);
        defaultMetadata.Should().NotBeEmpty();
        defaultMetadata.Keys.Should().BeEquivalentTo([type]);
        defaultMetadata.Values.First().Name.Should().Be(propertyName);
    }

    private class Blog
    {
        public string Title { get; set; }
    }
}
