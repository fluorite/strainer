﻿using Fluorite.Strainer.Services.Metadata;
using System.Linq.Expressions;
using System.Reflection;

namespace Fluorite.Strainer.UnitTests.Services.Metadata;

public class PropertyInfoProviderTests
{
    [Fact]
    public void Provider_Returns_PropertyInfo()
    {
        // Arrange
        var type = typeof(string);
        var name = nameof(string.Length);
        var provider = new PropertyInfoProvider();

        // Act
        var result = provider.GetPropertyInfo(type, name);

        // Assert
        result.Should().NotBeNull();
        result.Equals(type.GetProperty(name)).Should().BeTrue();
    }

    [Fact]
    public void Provider_Returns_PropertyInfoAndFullName()
    {
        // Arrange
        Expression<Func<Stub, object>> expression = s => s.Property;
        var provider = new PropertyInfoProvider();

        // Act
        var (propertyInfo, fullName) = provider.GetPropertyInfoAndFullName(expression);

        // Assert
        fullName.Should().Be(nameof(Stub.Property));
        propertyInfo.Should().NotBeNull();
        propertyInfo.Should().BeSameAs(typeof(Stub).GetProperty(nameof(Stub.Property)));
    }

    [Fact]
    public void Provider_Returns_PropertyInfoAndFullName_With_Only_Getter()
    {
        // Arrange
        Expression<Func<Stub, object>> expression = s => s.PropertyOnlyGetter;
        var provider = new PropertyInfoProvider();

        // Act
        var (propertyInfo, fullName) = provider.GetPropertyInfoAndFullName(expression);

        // Assert
        fullName.Should().Be(nameof(Stub.PropertyOnlyGetter));
        propertyInfo.Should().NotBeNull();
        propertyInfo.Should().BeSameAs(typeof(Stub).GetProperty(nameof(Stub.PropertyOnlyGetter)));
    }

    [Fact]
    public void Provider_Throws_For_Fields()
    {
        // Arrange
        Expression<Func<Stub, object>> expression = (Stub v) => v.Field;
        var provider = new PropertyInfoProvider();

        // Act & Assert
        Action result = () => provider.GetPropertyInfoAndFullName(expression);
        result.Should().ThrowExactly<ArgumentException>();
    }

    [Fact]
    public void Provider_Throws_For_Methods()
    {
        // Arrange
        Expression<Func<Stub, object>> expression = (Stub v) => v.Method();
        var provider = new PropertyInfoProvider();

        // Act & Assert
        Action result = () => provider.GetPropertyInfoAndFullName(expression);
        result.Should().ThrowExactly<ArgumentException>();
    }

    [Fact]
    public void Provider_Returns_PropertyInfos()
    {
        // Arrange
        var propertyInfoMock = Substitute.For<PropertyInfo>();
        var propertyInfos = new[] { propertyInfoMock };
        var typeMock = Substitute.For<Type>();
        typeMock.GetProperties(BindingFlags.Instance | BindingFlags.Public).Returns(propertyInfos);
        var provider = new PropertyInfoProvider();

        // Act
        var result = provider.GetPropertyInfos(typeMock);

        // Assert
        result.Should().NotBeNullOrEmpty();
        result.Should().HaveSameCount(propertyInfos);
        result.Should().BeSameAs(propertyInfos);
    }

    private class Stub
    {
#pragma warning disable SA1401 // Fields should be private
        public readonly int Field = 0;
#pragma warning restore SA1401 // Fields should be private

        public int Property { get; set; }

        public int PropertyOnlyGetter { get; }

        public int Method() => default;
    }
}
