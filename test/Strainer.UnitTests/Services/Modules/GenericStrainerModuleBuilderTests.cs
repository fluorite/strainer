﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Models.Filtering;
using Fluorite.Strainer.Models.Filtering.Operators;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Modules;
using System.Linq.Expressions;
using System.Reflection;

namespace Fluorite.Strainer.UnitTests.Services.Modules;

public class GenericStrainerModuleBuilderTests
{
    private readonly IPropertyInfoProvider _propertyInfoProviderMock = Substitute.For<IPropertyInfoProvider>();

    [Fact]
    public void Should_Add_SymbolToExcludedListOfBuiltInFilterOperator()
    {
        // Arrange
        var symbol = FilterOperatorSymbols.EqualsSymbol;
        var builtInSymbols = new HashSet<string>();
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.ExcludedBuiltInFilterOperators.Returns(builtInSymbols);
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        builder.RemoveBuiltInFilterOperator(FilterOperatorSymbols.EqualsSymbol);

        // Assert
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
        builtInSymbols.Should().BeEquivalentTo(symbol);
        _ = strainerModuleMock
            .Received(1)
            .ExcludedBuiltInFilterOperators;
    }

    [Fact]
    public void Should_Throw_WhenAddingObjectMetadata_WhileFluentApiIsDisabled()
    {
        // Arrange
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        var strainerOptions = new StrainerOptions
        {
            MetadataSourceType = MetadataSourceType.None,
        };
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        Action act = () => builder.AddObject(x => x.Id);

        // Assert
        act.Should().Throw<NotSupportedException>()
            .WithMessage(
                $"Current {nameof(MetadataSourceType)} setting does not " +
                $"support {nameof(MetadataSourceType.FluentApi)}. " +
                $"Include {nameof(MetadataSourceType.FluentApi)} option to " +
                $"be able to use it.");
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    [Fact]
    public void Should_Add_ObjectMetadata()
    {
        // Arrange
        var objectMetadata = new Dictionary<Type, IObjectMetadata>();
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.ObjectMetadata.Returns(objectMetadata);
        var defaultSortingName = "foo";
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);
        Expression<Func<Stub, object>> expression = x => x.Id;
        var propertyInfo = Substitute.For<PropertyInfo>();

        _propertyInfoProviderMock
            .GetPropertyInfoAndFullName(expression)
            .Returns((propertyInfo, defaultSortingName));

        // Act
        builder.AddObject(expression);

        // Assert
        objectMetadata.Should().NotBeEmpty();
        objectMetadata.Should().HaveCount(1);
        objectMetadata.Keys.First().Should().Be<Stub>();
        objectMetadata.Values.First().Should().NotBeNull();
        objectMetadata.Values.First().DefaultSortingPropertyName.Should().Be(defaultSortingName);
        _propertyInfoProviderMock
            .Received(1)
            .GetPropertyInfoAndFullName(expression);
        _ = strainerModuleMock
            .Received(1)
            .ObjectMetadata;
    }

    [Fact]
    public void Should_Throw_WhenAddingPropertyMetadata_WhileFluentApiIsDisabled()
    {
        // Arrange
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        var strainerOptions = new StrainerOptions
        {
            MetadataSourceType = MetadataSourceType.None,
        };
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        Action act = () => builder.AddProperty(x => x.Id);

        // Assert
        act.Should().Throw<NotSupportedException>()
            .WithMessage(
                $"Current {nameof(MetadataSourceType)} setting does not " +
                $"support {nameof(MetadataSourceType.FluentApi)}. " +
                $"Include {nameof(MetadataSourceType.FluentApi)} option to " +
                $"be able to use it.");
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    [Fact]
    public void Should_Add_PropertyMetadata()
    {
        // Arrange
        var propertyMetadata = new Dictionary<Type, IDictionary<string, IPropertyMetadata>>();
        var defaultMetadata = new Dictionary<Type, IPropertyMetadata>();
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.PropertyMetadata.Returns(propertyMetadata);
        strainerModuleMock.DefaultMetadata.Returns(defaultMetadata);
        var propertyName = "foo";
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);
        Expression<Func<Stub, object>> expression = x => x.Id;
        var propertyInfo = Substitute.For<PropertyInfo>();

        _propertyInfoProviderMock
            .GetPropertyInfoAndFullName(expression)
            .Returns((propertyInfo, propertyName));

        // Act
        builder.AddProperty(expression);

        // Assert
        propertyMetadata.Should().NotBeEmpty();
        propertyMetadata.Should().HaveCount(1);
        propertyMetadata.Keys.First().Should().Be<Stub>();
        propertyMetadata.Values.First().Should().NotBeNullOrEmpty();
        propertyMetadata.Values.First().Should().HaveCount(1);
        propertyMetadata.Values.First().Keys.Should().BeEquivalentTo(propertyName);
        propertyMetadata.Values.First().Values.First().Should().NotBeNull();
        propertyMetadata.Values.First().Values.First().Name.Should().Be(propertyName);
        propertyMetadata.Values.First().Values.First().PropertyInfo.Should().BeSameAs(propertyInfo);
        defaultMetadata.Should().BeEmpty();
        _propertyInfoProviderMock
            .Received(1)
            .GetPropertyInfoAndFullName(expression);
        _ = strainerModuleMock
            .Received(1)
            .PropertyMetadata;
        _ = strainerModuleMock
            .Received(1)
            .DefaultMetadata;
    }

    [Fact]
    public void Should_Throw_WhenAddingFilterOperator_AndFilterSymbolIsAlreadyTaken()
    {
        // Arrange
        var symbol = "foo";
        var operatorName = "name";
        var expression = Expression.Empty();
        var sortOperator = Substitute.For<IFilterOperator>();
        sortOperator.Symbol.Returns(symbol);
        var sortOperators = new Dictionary<string, IFilterOperator>()
        {
            [symbol] = Substitute.For<IFilterOperator>(),
        };
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.FilterOperators.Returns(sortOperators);
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        Action act = () => builder.AddFilterOperator(x => x
            .HasSymbol(symbol)
            .HasName(operatorName)
            .HasExpression(_ => expression)
            .Build());

        // Assert
        act.Should().Throw<InvalidOperationException>()
            .WithMessage(
                $"There is an already existing operator with a symbol {sortOperator.Symbol}. " +
                $"Please, choose a different symbol.");
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    [Fact]
    public void Should_Add_FilterOperator()
    {
        // Arrange
        var symbol = "foo";
        var operatorName = "name";
        var expression = Expression.Empty();
        var sortOperator = Substitute.For<IFilterOperator>();
        sortOperator.Symbol.Returns(symbol);
        var sortOperators = new Dictionary<string, IFilterOperator>();
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.FilterOperators.Returns(sortOperators);
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        builder.AddFilterOperator(x => x
            .HasSymbol(symbol)
            .HasName(operatorName)
            .HasExpression(_ => expression)
            .Build());

        // Assert
        sortOperators.Should().NotBeEmpty();
        sortOperators.Should().HaveCount(1);
        sortOperators.Keys.Should().BeEquivalentTo(symbol);
        sortOperators.Values.First().Should().NotBeNull();
        sortOperators.Values.First().Symbol.Should().Be(symbol);
        sortOperators.Values.First().Name.Should().Be(operatorName);
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    [Fact]
    public void Should_Add_CustomFilterMethod()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Stub, bool>> expression = x => x.Id == 0;
        var customFilterMethods = new Dictionary<Type, IDictionary<string, ICustomFilterMethod>>();
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.CustomFilterMethods.Returns(customFilterMethods);
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        builder.AddCustomFilterMethod(x => x.HasName(name).HasFunction(expression).Build());

        // Assert
        customFilterMethods.Should().NotBeEmpty();
        customFilterMethods.Should().HaveCount(1);
        customFilterMethods.Keys.First().Should().Be<Stub>();
        customFilterMethods.Values.Should().NotBeNullOrEmpty();
        customFilterMethods.Values.Should().HaveCount(1);
        customFilterMethods.Values.First().Should().NotBeNull();
        customFilterMethods.Values.First().Keys.Should().BeEquivalentTo(name);
        customFilterMethods.Values.First().Values.First().Should().NotBeNull();
        customFilterMethods.Values.First().Values.First().Name.Should().Be(name);
        customFilterMethods.Values.First().Values.First().Should().BeAssignableTo<ICustomFilterMethod<Stub>>()
            .Subject.Expression.Should().Be(expression);
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    [Fact]
    public void Should_Add_CustomSortMethod()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Stub, object>> expression = x => x.Id;
        var customSortMethods = new Dictionary<Type, IDictionary<string, ICustomSortMethod>>();
        var strainerModuleMock = Substitute.For<IStrainerModule>();
        strainerModuleMock.CustomSortMethods.Returns(customSortMethods);
        var strainerOptions = new StrainerOptions();
        var builder = new StrainerModuleBuilder<Stub>(
            _propertyInfoProviderMock,
            strainerModuleMock,
            strainerOptions);

        // Act
        builder.AddCustomSortMethod(x => x.HasName(name).HasFunction(expression).Build());

        // Assert
        customSortMethods.Should().NotBeEmpty();
        customSortMethods.Should().HaveCount(1);
        customSortMethods.Keys.First().Should().Be<Stub>();
        customSortMethods.Values.Should().NotBeNullOrEmpty();
        customSortMethods.Values.Should().HaveCount(1);
        customSortMethods.Values.First().Should().NotBeNull();
        customSortMethods.Values.First().Keys.Should().BeEquivalentTo(name);
        customSortMethods.Values.First().Values.First().Should().NotBeNull();
        customSortMethods.Values.First().Values.First().Name.Should().Be(name);
        customSortMethods.Values.First().Values.First().Should().BeAssignableTo<ICustomSortMethod<Stub>>()
            .Subject.Expression.Should().Be(expression);
        _propertyInfoProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    private class Stub
    {
        public int Id { get; set; }
    }
}
