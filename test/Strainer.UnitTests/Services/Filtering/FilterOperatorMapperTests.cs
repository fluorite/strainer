﻿using Fluorite.Strainer.Models.Filtering.Operators;
using Fluorite.Strainer.Services.Filtering;
using System.Linq.Expressions;
using static Fluorite.Strainer.Services.Filtering.FilterOperatorMapper;

namespace Fluorite.Strainer.UnitTests.Services.Filtering;

public class FilterOperatorMapperTests
{
    [Theory]
    [InlineData(null,   null,   false,  false,  true)]
    [InlineData("foo",  null,   false,  false,  false)]
    [InlineData(null,   "foo",  false,  false,  false)]
    [InlineData("",     "",     false,  false,  true)]
    [InlineData(" ",    " ",    false,  false,  true)]
    [InlineData("123",  "123",  false,  false,  true)]
    [InlineData("foo",  "foo",  false,  false,  true)]
    [InlineData("foo",  "FOO",  false,  false,  false)]
    [InlineData("FOO",  "foo",  false,  false,  false)]
    [InlineData("foo",  "bar",  false,  false,  false)]
    [InlineData(null,   null,   true,   true,   true)]
    [InlineData("foo",  null,   true,   true,   false)]
    [InlineData(null,   "foo",  true,   true,   false)]
    [InlineData("",     "",     true,   true,   true)]
    [InlineData(" ",    " ",    true,   true,   true)]
    [InlineData("123", "123",   true,   true,   true)]
    [InlineData("foo", "foo",   true,   true,   true)]
    [InlineData("foo", "FOO",   true,   true,   true)]
    [InlineData("FOO", "foo",   true,   true,   true)]
    [InlineData("foo", "bar",   true,   true,   false)]
    public void Should_work_on_equal_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.EqualsSymbol;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null,   null,   false,  false,  false)]
    [InlineData("foo",  null,   false,  false,  true)]
    [InlineData(null,   "foo",  false,  false,  true)]
    [InlineData("",     "",     false,  false,  false)]
    [InlineData(" ",    " ",    false,  false,  false)]
    [InlineData("123",  "123",  false,  false,  false)]
    [InlineData("foo",  "foo",  false,  false,  false)]
    [InlineData("foo",  "FOO",  false,  false,  true)]
    [InlineData("Foo",  "foo",  false,  false,  true)]
    [InlineData("foo",  "bar",  false,  false,  true)]
    [InlineData(null,   null,   true,   true,   false)]
    [InlineData("foo",  null,   true,   true,   true)]
    [InlineData(null,   "foo",  true,   true,   true)]
    [InlineData("",     "",     true,   true,   false)]
    [InlineData(" ",    " ",    true,   true,   false)]
    [InlineData("123",  "123",  true,   true,   false)]
    [InlineData("foo",  "foo",  true,   true,   false)]
    [InlineData("foo",  "FOO",  true,   true,   false)]
    [InlineData("FOO",  "foo",  true,   true,   false)]
    [InlineData("foo",  "bar",  true,   true,   true)]
    public void Should_work_on_does_not_equal_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotEqual;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null, null, false)]
    [InlineData(1000, null, false)]
    [InlineData(null, 1000, false)]
    [InlineData(1000, 1000, false)]
    [InlineData(1000, 9999, false)]
    [InlineData(9999, 1000, true)]
    public void Should_work_on_greater_than_operator(int? property, int? filter, bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.GreaterThan;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null, null, false)]
    [InlineData(1000, null, false)]
    [InlineData(null, 1000, false)]
    [InlineData(1000, 1000, true)]
    [InlineData(1000, 9999, false)]
    [InlineData(9999, 1000, true)]
    public void Should_work_on_greater_than_or_equal_to_operator(int? property, int? filter, bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.GreaterThanOrEqualTo;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null, null, false)]
    [InlineData(1000, null, false)]
    [InlineData(null, 1000, false)]
    [InlineData(1000, 1000, false)]
    [InlineData(1000, 9999, true)]
    [InlineData(9999, 1000, false)]
    public void Should_work_on_less_than_operator(int? property, int? filter, bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.LessThan;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null, null, false)]
    [InlineData(1000, null, false)]
    [InlineData(null, 1000, false)]
    [InlineData(1000, 1000, true)]
    [InlineData(1000, 9999, true)]
    [InlineData(9999, 1000, false)]
    public void Should_work_on_less_than_or_equal_to_operator(int? property, int? filter, bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.LessThanOrEqualTo;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false,  true)]
    [InlineData(" ",        " ",    false,  false,  true)]
    [InlineData("123",      "123",  false,  false,  true)]
    [InlineData("foo",      "foo",  false,  false,  true)]
    [InlineData("foo",      "FOO",  false,  false,  false)]
    [InlineData("Foo",      "foo",  false,  false,  false)]
    [InlineData("foo",      "bar",  false,  false,  false)]
    [InlineData("foobar",   "foo",  false,  false,  true)]
    [InlineData("barfoo",   "foo",  false,  false,  true)]
    [InlineData("",         "",     true,   true,   true)]
    [InlineData(" ",        " ",    true,   true,   true)]
    [InlineData("123",      "123",  true,   true,   true)]
    [InlineData("foo",      "foo",  true,   true,   true)]
    [InlineData("foo",      "FOO",  true,   true,   true)]
    [InlineData("FOO",      "foo",  true,   true,   true)]
    [InlineData("foo",      "bar",  true,   true,   false)]
    [InlineData("foobar",   "foo",  true,   true,   true)]
    [InlineData("barfoo",   "foo",  true,   true,   true)]
    public void Should_work_on_contains_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.Contains;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false,  false)]
    [InlineData(" ",        " ",    false,  false,  false)]
    [InlineData("123",      "123",  false,  false,  false)]
    [InlineData("foo",      "foo",  false,  false,  false)]
    [InlineData("foo",      "FOO",  false,  false,  true)]
    [InlineData("FOO",      "foo",  false,  false,  true)]
    [InlineData("foo",      "bar",  false,  false,  true)]
    [InlineData("foobar",   "foo",  false,  false,  false)]
    [InlineData("barfoo",   "foo",  false,  false,  false)]
    [InlineData("",         "",     true,   true,   false)]
    [InlineData(" ",        " ",    true,   true,   false)]
    [InlineData("123",      "123",  true,   true,   false)]
    [InlineData("foo",      "foo",  true,   true,   false)]
    [InlineData("foo",      "FOO",  true,   true,   false)]
    [InlineData("FOO",      "foo",  true,   true,   false)]
    [InlineData("foo",      "bar",  true,   true,   true)]
    [InlineData("foobar",   "foo",  true,   true,   false)]
    [InlineData("barfoo",   "foo",  true,   true,   false)]
    public void Should_work_on_does_not_contain_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotContain;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false,  true)]
    [InlineData(" ",        " ",    false,  false,  true)]
    [InlineData("123",      "123",  false,  false,  true)]
    [InlineData("foo",      "foo",  false,  false,  true)]
    [InlineData("foo",      "FOO",  false,  false,  false)]
    [InlineData("FOO",      "foo",  false,  false,  false)]
    [InlineData("foo",      "bar",  false,  false,  false)]
    [InlineData("foobar",   "foo",  false,  false,  true)]
    [InlineData("barfoo",   "foo",  false,  false,  false)]
    [InlineData("",         "",     true,   true,   true)]
    [InlineData(" ",        " ",    true,   true,   true)]
    [InlineData("123",      "123",  true,   true,   true)]
    [InlineData("foo",      "foo",  true,   true,   true)]
    [InlineData("foo",      "FOO",  true,   true,   true)]
    [InlineData("FOO",      "foo",  true,   true,   true)]
    [InlineData("foo",      "bar",  true,   true,   false)]
    [InlineData("foobar",   "foo",  true,   true,   true)]
    [InlineData("barfoo",   "foo",  true,   true,   false)]
    public void Should_work_on_starts_with_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.StartsWith;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false,  false)]
    [InlineData(" ",        " ",    false,  false,  false)]
    [InlineData("123",      "123",  false,  false,  false)]
    [InlineData("foo",      "foo",  false,  false,  false)]
    [InlineData("foo",      "FOO",  false,  false,  true)]
    [InlineData("FOO",      "foo",  false,  false,  true)]
    [InlineData("foo",      "bar",  false,  false,  true)]
    [InlineData("foobar",   "foo",  false,  false,  false)]
    [InlineData("barfoo",   "foo",  false,  false,  true)]
    [InlineData("",         "",     true,   true,   false)]
    [InlineData(" ",        " ",    true,   true,   false)]
    [InlineData("123",      "123",  true,   true,   false)]
    [InlineData("foo",      "foo",  true,   true,   false)]
    [InlineData("foo",      "FOO",  true,   true,   false)]
    [InlineData("FOO",      "foo",  true,   true,   false)]
    [InlineData("foo",      "bar",  true,   true,   true)]
    [InlineData("foobar",   "foo",  true,   true,   false)]
    [InlineData("barfoo",   "foo",  true,   true,   true)]
    public void Should_work_on_does_not_start_with_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotStartWith;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false,  true)]
    [InlineData(" ",        " ",    false,  false,  true)]
    [InlineData("123",      "123",  false,  false,  true)]
    [InlineData("foo",      "foo",  false,  false,  true)]
    [InlineData("foo",      "FOO",  false,  false,  false)]
    [InlineData("FOO",      "foo",  false,  false,  false)]
    [InlineData("foo",      "bar",  false,  false,  false)]
    [InlineData("foobar",   "foo",  false,  false,  false)]
    [InlineData("barfoo",   "foo",  false,  false,  true)]
    [InlineData("",         "",     true,   true,   true)]
    [InlineData(" ",        " ",    true,   true,   true)]
    [InlineData("123",      "123",  true,   true,   true)]
    [InlineData("foo",      "foo",  true,   true,   true)]
    [InlineData("foo",      "FOO",  true,   true,   true)]
    [InlineData("FOO",      "foo",  true,   true,   true)]
    [InlineData("foo",      "bar",  true,   true,   false)]
    [InlineData("foobar",   "foo",  true,   true,   false)]
    [InlineData("barfoo",   "foo",  true,   true,   true)]
    public void Should_work_on_ends_with_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.EndsWith;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false,  false)]
    [InlineData(" ",        " ",    false,  false,  false)]
    [InlineData("123",      "123",  false,  false,  false)]
    [InlineData("foo",      "foo",  false,  false,  false)]
    [InlineData("foo",      "FOO",  false,  false,  true)]
    [InlineData("FOO",      "foo",  false,  false,  true)]
    [InlineData("foo",      "bar",  false,  false,  true)]
    [InlineData("foobar",   "foo",  false,  false,  true)]
    [InlineData("barfoo",   "foo",  false,  false,  false)]
    [InlineData("",         "",     true,   true,   false)]
    [InlineData(" ",        " ",    true,   true,   false)]
    [InlineData("123",      "123",  true,   true,   false)]
    [InlineData("foo",      "foo",  true,   true,   false)]
    [InlineData("foo",      "FOO",  true,   true,   false)]
    [InlineData("FOO",      "foo",  true,   true,   false)]
    [InlineData("foo",      "bar",  true,   true,   true)]
    [InlineData("foobar",   "foo",  true,   true,   true)]
    [InlineData("barfoo",   "foo",  true,   true,   false)]
    public void Should_work_on_does_not_end_with_operator(
        string property,
        string filter,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotEndWith;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isCaseInsensitiveForValues, isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  true)]
    [InlineData(" ",        " ",    false,  true)]
    [InlineData("123",      "123",  false,  true)]
    [InlineData("foo",      "foo",  false,  true)]
    [InlineData("foo",      "FOO",  false,  false)]
    [InlineData("FOO",      "foo",  false,  false)]
    [InlineData("foo",      "bar",  false,  false)]
    [InlineData("foobar",   "foo",  false,  true)]
    [InlineData("barfoo",   "foo",  false,  true)]
    [InlineData("",         "",     true,   true)]
    [InlineData(" ",        " ",    true,   true)]
    [InlineData("123",      "123",  true,   true)]
    [InlineData("foo",      "foo",  true,   true)]
    [InlineData("foo",      "FOO",  true,   true)]
    [InlineData("FOO",      "foo",  true,   true)]
    [InlineData("foo",      "bar",  true,   false)]
    [InlineData("foobar",   "foo",  true,   true)]
    [InlineData("barfoo",   "foo",  true,   true)]
    public void Should_work_on_contains_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.ContainsCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false)]
    [InlineData(" ",        " ",    false,  false)]
    [InlineData("123",      "123",  false,  false)]
    [InlineData("foo",      "foo",  false,  false)]
    [InlineData("foo",      "FOO",  false,  true)]
    [InlineData("FOO",      "foo",  false,  true)]
    [InlineData("foo",      "bar",  false,  true)]
    [InlineData("foobar",   "foo",  false,  false)]
    [InlineData("barfoo",   "foo",  false,  false)]
    [InlineData("",         "",     true,   false)]
    [InlineData(" ",        " ",    true,   false)]
    [InlineData("123",      "123",  true,   false)]
    [InlineData("foo",      "foo",  true,   false)]
    [InlineData("foo",      "FOO",  true,   false)]
    [InlineData("FOO",      "foo",  true,   false)]
    [InlineData("foo",      "bar",  true,   true)]
    [InlineData("foobar",   "foo",  true,   false)]
    [InlineData("barfoo",   "foo",  true,   false)]
    public void Should_work_on_does_not_contain_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotContainCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  true)]
    [InlineData(" ",        " ",    false,  true)]
    [InlineData("123",      "123",  false,  true)]
    [InlineData("foo",      "foo",  false,  true)]
    [InlineData("foo",      "FOO",  false,  false)]
    [InlineData("FOO",      "foo",  false,  false)]
    [InlineData("foo",      "bar",  false,  false)]
    [InlineData("foobar",   "foo",  false,  true)]
    [InlineData("barfoo",   "foo",  false,  false)]
    [InlineData("",         "",     true,   true)]
    [InlineData(" ",        " ",    true,   true)]
    [InlineData("123",      "123",  true,   true)]
    [InlineData("foo",      "foo",  true,   true)]
    [InlineData("foo",      "FOO",  true,   true)]
    [InlineData("FOO",      "foo",  true,   true)]
    [InlineData("foo",      "bar",  true,   false)]
    [InlineData("foobar",   "foo",  true,   true)]
    [InlineData("barfoo",   "foo",  true,   false)]
    public void Should_work_on_starts_with_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.StartsWithCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false)]
    [InlineData(" ",        " ",    false,  false)]
    [InlineData("123",      "123",  false,  false)]
    [InlineData("foo",      "foo",  false,  false)]
    [InlineData("foo",      "FOO",  false,  true)]
    [InlineData("FOO",      "foo",  false,  true)]
    [InlineData("foo",      "bar",  false,  true)]
    [InlineData("foobar",   "foo",  false,  false)]
    [InlineData("barfoo",   "foo",  false,  true)]
    [InlineData("",         "",     true,   false)]
    [InlineData(" ",        " ",    true,   false)]
    [InlineData("123",      "123",  true,   false)]
    [InlineData("foo",      "foo",  true,   false)]
    [InlineData("foo",      "FOO",  true,   false)]
    [InlineData("FOO",      "foo",  true,   false)]
    [InlineData("foo",      "bar",  true,   true)]
    [InlineData("foobar",   "foo",  true,   false)]
    [InlineData("barfoo",   "foo",  true,   true)]
    public void Should_work_on_does_not_start_with_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotStartWithCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  true)]
    [InlineData(" ",        " ",    false,  true)]
    [InlineData("123",      "123",  false,  true)]
    [InlineData("foo",      "foo",  false,  true)]
    [InlineData("foo",      "FOO",  false,  false)]
    [InlineData("FOO",      "foo",  false,  false)]
    [InlineData("foo",      "bar",  false,  false)]
    [InlineData("foobar",   "foo",  false,  false)]
    [InlineData("barfoo",   "foo",  false,  true)]
    [InlineData("",         "",     true,   true)]
    [InlineData(" ",        " ",    true,   true)]
    [InlineData("123",      "123",  true,   true)]
    [InlineData("foo",      "foo",  true,   true)]
    [InlineData("foo",      "FOO",  true,   true)]
    [InlineData("FOO",      "foo",  true,   true)]
    [InlineData("foo",      "bar",  true,   false)]
    [InlineData("foobar",   "foo",  true,   false)]
    [InlineData("barfoo",   "foo",  true,   true)]
    public void Should_work_on_ends_with_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.EndsWithCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData("",         "",     false,  false)]
    [InlineData(" ",        " ",    false,  false)]
    [InlineData("123",      "123",  false,  false)]
    [InlineData("foo",      "foo",  false,  false)]
    [InlineData("foo",      "FOO",  false,  true)]
    [InlineData("FOO",      "foo",  false,  true)]
    [InlineData("foo",      "bar",  false,  true)]
    [InlineData("foobar",   "foo",  false,  true)]
    [InlineData("barfoo",   "foo",  false,  false)]
    [InlineData("",         "",     true,   false)]
    [InlineData(" ",        " ",    true,   false)]
    [InlineData("123",      "123",  true,   false)]
    [InlineData("foo",      "foo",  true,   false)]
    [InlineData("foo",      "FOO",  true,   false)]
    [InlineData("FOO",      "foo",  true,   false)]
    [InlineData("foo",      "bar",  true,   true)]
    [InlineData("foobar",   "foo",  true,   true)]
    [InlineData("barfoo",   "foo",  true,   false)]
    public void Should_work_on_does_not_end_with_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotEndWithCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null,   null,   false,  true)]
    [InlineData("foo",  null,   false,  false)]
    [InlineData(null,   "foo",  false,  false)]
    [InlineData("",     "",     false,  true)]
    [InlineData(" ",    " ",    false,  true)]
    [InlineData("123",  "123",  false,  true)]
    [InlineData("foo",  "foo",  false,  true)]
    [InlineData("foo",  "FOO",  false,  false)]
    [InlineData("FOO",  "foo",  false,  false)]
    [InlineData("foo",  "bar",  false,  false)]
    [InlineData(null,   null,   true,   true)]
    [InlineData("foo",  null,   true,   false)]
    [InlineData(null,   "foo",  true,   false)]
    [InlineData("",     "",     true,   true)]
    [InlineData(" ",    " ",    true,   true)]
    [InlineData("123",  "123",  true,   true)]
    [InlineData("foo",  "foo",  true,   true)]
    [InlineData("foo",  "FOO",  true,   true)]
    [InlineData("FOO",  "foo",  true,   true)]
    [InlineData("foo",  "bar",  true,   false)]
    public void Should_work_on_equal_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.EqualsCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    [Theory]
    [InlineData(null,   null,   false,  false)]
    [InlineData("foo",  null,   false,  true)]
    [InlineData(null,   "foo",  false,  true)]
    [InlineData("",     "",     false,  false)]
    [InlineData(" ",    " ",    false,  false)]
    [InlineData("123",  "123",  false,  false)]
    [InlineData("foo",  "foo",  false,  false)]
    [InlineData("foo",  "FOO",  false,  true)]
    [InlineData("FOO",  "foo",  false,  true)]
    [InlineData("foo",  "bar",  false,  true)]
    [InlineData(null,   null,   true,   false)]
    [InlineData("foo",  null,   true,   true)]
    [InlineData(null,   "foo",  true,   true)]
    [InlineData("",     "",     true,   false)]
    [InlineData(" ",    " ",    true,   false)]
    [InlineData("123",  "123",  true,   false)]
    [InlineData("foo",  "foo",  true,   false)]
    [InlineData("foo",  "FOO",  true,   false)]
    [InlineData("FOO",  "foo",  true,   false)]
    [InlineData("foo",  "bar",  true,   true)]
    public void Should_work_on_does_not_equal_case_insensitive_operator(
        string property,
        string filter,
        bool isMaterializedQueryable,
        bool expectedEqual)
    {
        // Arrange
        var operatorSymbol = FilterOperatorSymbols.DoesNotEqualCaseInsensitive;
        var lambda = BuildLambdaExpression(property, filter, operatorSymbol, isMaterializedQueryable: isMaterializedQueryable);

        // Act
        var func = lambda.Compile();
        var result = func.Invoke();

        // Assert
        result.Should().Be(expectedEqual);
    }

    private static Expression<Func<bool>> BuildLambdaExpression<T>(
        T property,
        T filter,
        string operatorSymbol,
        bool isCaseInsensitiveForValues = false,
        bool isMaterializedQueryable = true)
    {
        var filterOperator = DefaultOperators[operatorSymbol];
        var propertyValue = Expression.Constant(property, typeof(T));
        var filterValue = Expression.Constant(filter, typeof(T));
        var filterExpressionContext = new FilterExpressionContext(
            filterValue,
            propertyValue,
            isCaseInsensitiveForValues,
            isMaterializedQueryable,
            isStringBasedProperty: typeof(T) == typeof(string));
        var expression = filterOperator.ExpressionProvider.Invoke(filterExpressionContext);

        return Expression.Lambda<Func<bool>>(expression);
    }
}
