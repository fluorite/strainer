﻿using Fluorite.Strainer.Services.Filtering;
using System.Linq.Expressions;

namespace Fluorite.Strainer.UnitTests.Services.Filtering;

public class CustomFilterMethodBuilderTests
{
    [Fact]
    public void Should_Build_CustomFilterMethod_UsingDirectExpression()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Post, bool>> expression1 = x => x.Author != null;
        var builder = new CustomFilterMethodBuilder<Post>()
            .HasName(name)
            .HasFunction(expression1);

        // Act
        var result = builder.Build();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().BeSameAs(expression1);
        result.Name.Should().Be(name);
        result.FilterTermExpression.Should().BeNull();
    }

    [Fact]
    public void Should_Build_CustomFilterMethod_UsingProviderExpression()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Post, bool>> expression1 = x => x.Author != null;
        var builder = new CustomFilterMethodBuilder<Post>()
            .HasName(name)
            .HasFunction(_ => expression1);

        // Act
        var result = builder.Build();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().BeNull();
        result.Name.Should().Be(name);
        result.FilterTermExpression.Should().NotBeNull();
        result.FilterTermExpression.Invoke(null).Should().BeSameAs(expression1);
    }

    [Fact]
    public void Should_Build_CustomFilterMethod_UsingDirectExpression_ErasingPreviousExpression()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Post, bool>> expression1 = x => x.Author != null;
        Expression<Func<Post, bool>> expression2 = x => x.Name != null;
        var builder = new CustomFilterMethodBuilder<Post>()
            .HasName(name)
            .HasFunction(expression1)
            .HasFunction(expression2);

        // Act
        var result = builder.Build();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().BeSameAs(expression2);
        result.Name.Should().BeSameAs(name);
        result.FilterTermExpression.Should().BeNull();
    }

    private class Post
    {
        public string Author { get; set; }

        public string Name { get; set; }
    }
}
