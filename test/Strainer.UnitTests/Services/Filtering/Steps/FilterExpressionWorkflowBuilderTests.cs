﻿using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Filtering.Steps;

namespace Fluorite.Strainer.UnitTests.Services.Filtering.Steps;

public class FilterExpressionWorkflowBuilderTests
{
    [Fact]
    public void Should_Return_Workflow()
    {
        // Arrange
        var builder = new FilterExpressionWorkflowBuilder(
            Substitute.For<IConvertPropertyValueToStringStep>(),
            Substitute.For<IConvertFilterValueToStringStep>(),
            Substitute.For<IChangeTypeOfFilterValueStep>(),
            Substitute.For<IApplyConsantClosureToFilterValueStep>(),
            Substitute.For<IMitigateCaseInsensitivityStep>(),
            Substitute.For<IApplyFilterOperatorStep>());

        // Act
        var result = builder.BuildDefaultWorkflow();

        // Assert
        result.Should().NotBeNull();
        result.Should().BeOfType<FilterExpressionWorkflow>();
    }
}
