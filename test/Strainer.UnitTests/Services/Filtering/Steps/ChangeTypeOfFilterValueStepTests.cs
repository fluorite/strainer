﻿using Fluorite.Strainer.Models.Filtering.Operators;
using Fluorite.Strainer.Models.Filtering.Terms;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Services.Conversion;
using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Filtering.Steps;
using System.Linq.Expressions;
using System.Reflection;

namespace Fluorite.Strainer.UnitTests.Services.Filtering.Steps;

public class ChangeTypeOfFilterValueStepTests
{
    private readonly ITypeChanger _typeChangerMock = Substitute.For<ITypeChanger>();
    private readonly ITypeConverterProvider _typeConverterProviderMock = Substitute.For<ITypeConverterProvider>();

    private readonly ChangeTypeOfFilterValueStep _step;

    public ChangeTypeOfFilterValueStepTests()
    {
        _step = new ChangeTypeOfFilterValueStep(
            _typeChangerMock,
            _typeConverterProviderMock);
    }

    [Fact]
    public void Should_DoNothing_When_FilterTermOperator_IsStringBased()
    {
        // Arrange
        var filterOperator = Substitute.For<IFilterOperator>();
        filterOperator.IsStringBased.Returns(true);
        var term = Substitute.For<IFilterTerm>();
        term.Operator.Returns(filterOperator);
        var propertyInfo = Substitute.For<PropertyInfo>();
        var propertyMetadata = Substitute.For<IPropertyMetadata>();
        propertyMetadata.PropertyInfo.Returns(propertyInfo);
        var context = new FilterExpressionWorkflowContext
        {
            Term = term,
            PropertyMetadata = propertyMetadata,
            FilterTermValue = "test",
        };

        // Act
        _step.Execute(context);

        // Assert
        _typeChangerMock
            .DidNotReceive()
            .ChangeType(Arg.Any<string>(), Arg.Any<Type>());
        _typeConverterProviderMock.ReceivedCalls().Should().BeEmpty();
    }

    [Fact]
    public void Should_Convert_When_PropertyType_IsString()
    {
        // Arrange
        var filterValue = "foo";
        var filterOperator = Substitute.For<IFilterOperator>();
        filterOperator.IsStringBased.Returns(false);
        var term = Substitute.For<IFilterTerm>();
        term.Operator.Returns(filterOperator);
        var propertyType = typeof(string);
        var propertyInfo = Substitute.For<PropertyInfo>();
        propertyInfo.PropertyType.Returns(propertyType);
        var propertyMetadata = Substitute.For<IPropertyMetadata>();
        propertyMetadata.PropertyInfo.Returns(propertyInfo);
        var typeConverter = Substitute.For<ITypeConverter>();
        var context = new FilterExpressionWorkflowContext
        {
            FilterTermValue = filterValue,
            PropertyMetadata = propertyMetadata,
            Term = term,
        };
        var typeChangingResult = Expression.Constant("bar");
        _typeChangerMock
            .ChangeType(filterValue, propertyType)
            .Returns(typeChangingResult);

        // Act
        _step.Execute(context);

        // Assert
        context.FilterTermConstant.Should().NotBeNull();
        context.FilterTermConstant.Should().Be(typeChangingResult);

        _typeChangerMock
            .Received(1)
            .ChangeType(filterValue, propertyType);
        _typeConverterProviderMock
            .Received(1)
            .GetTypeConverter(propertyType);
        typeConverter
            .DidNotReceive()
            .CanConvertFrom(typeof(string));
    }

    [Fact]
    public void Should_Covert_When_TypeConverter_CannotConvertFromString()
    {
        // Arrange
        var filterValue = "foo";
        var filterOperator = Substitute.For<IFilterOperator>();
        filterOperator.IsStringBased.Returns(false);
        var term = Substitute.For<IFilterTerm>();
        term.Operator.Returns(filterOperator);
        var propertyType = typeof(int);
        var propertyInfo = Substitute.For<PropertyInfo>();
        propertyInfo.PropertyType.Returns(propertyType);
        var propertyMetadata = Substitute.For<IPropertyMetadata>();
        propertyMetadata.PropertyInfo.Returns(propertyInfo);
        var context = new FilterExpressionWorkflowContext
        {
            FilterTermValue = filterValue,
            PropertyMetadata = propertyMetadata,
            Term = term,
        };
        var typeChangingResult = Expression.Constant("bar");

        _typeChangerMock
            .ChangeType(filterValue, propertyType)
            .Returns(typeChangingResult);
        var typeConverter = Substitute.For<ITypeConverter>();
        typeConverter
            .CanConvertFrom(typeof(string))
            .Returns(false);
        _typeConverterProviderMock
            .GetTypeConverter(propertyType)
            .Returns(typeConverter);

        // Act
        _step.Execute(context);

        // Assert
        context.FilterTermConstant.Should().NotBeNull();
        context.FilterTermConstant.Should().Be(typeChangingResult);

        _typeChangerMock
            .Received(1)
            .ChangeType(filterValue, propertyType);
        _typeConverterProviderMock
            .Received(1)
            .GetTypeConverter(propertyType);
        typeConverter
            .Received(1)
            .CanConvertFrom(typeof(string));
    }

    [Fact]
    public void Should_DoNothing_When_CanConvertFromString_AndPropertyTypeIsNotString()
    {
        // Arrange
        var filterOperator = Substitute.For<IFilterOperator>();
        filterOperator.IsStringBased.Returns(false);
        var term = Substitute.For<IFilterTerm>();
        term.Operator.Returns(filterOperator);
        var propertyType = typeof(int);
        var propertyInfo = Substitute.For<PropertyInfo>();
        propertyInfo.PropertyType.Returns(propertyType);
        var propertyMetadata = Substitute.For<IPropertyMetadata>();
        propertyMetadata.PropertyInfo.Returns(propertyInfo);
        var typeConverter = Substitute.For<ITypeConverter>();
        typeConverter.CanConvertFrom(typeof(string)).Returns(true);
        var context = new FilterExpressionWorkflowContext
        {
            PropertyMetadata = propertyMetadata,
            Term = term,
            FilterTermValue = "test",
        };

        _typeConverterProviderMock
            .GetTypeConverter(propertyType)
            .Returns(typeConverter);

        // Act
        _step.Execute(context);

        // Assert
        context.FilterTermConstant.Should().BeNull();

        _typeChangerMock
            .DidNotReceive()
            .ChangeType(Arg.Any<string>(), Arg.Any<Type>());
        _typeConverterProviderMock
            .Received(1)
            .GetTypeConverter(propertyType);
        typeConverter
            .Received(1)
            .CanConvertFrom(typeof(string));
    }
}
