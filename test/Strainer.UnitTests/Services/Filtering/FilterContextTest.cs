﻿using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Validation;

namespace Fluorite.Strainer.UnitTests.Services.Filtering;

public class FilterContextTest
{
    [Fact]
    public void Should_Create_FilterContext()
    {
        // Arrange
        var filterExpressionProvider = Substitute.For<IFilterExpressionProvider>();
        var operatorParser = Substitute.For<IFilterOperatorParser>();
        var operatorValidator = Substitute.For<IFilterOperatorValidator>();
        var filterTermParser = Substitute.For<IFilterTermParser>();

        // Act
        var result = new FilterContext(
            filterExpressionProvider,
            operatorParser,
            operatorValidator,
            filterTermParser);

        // Assert
        result.ExpressionProvider.Should().BeSameAs(filterExpressionProvider);
        result.OperatorParser.Should().BeSameAs(operatorParser);
        result.OperatorValidator.Should().BeSameAs(operatorValidator);
        result.TermParser.Should().BeSameAs(filterTermParser);
    }
}
