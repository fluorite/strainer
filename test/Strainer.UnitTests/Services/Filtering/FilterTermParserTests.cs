﻿using Fluorite.Strainer.Models.Configuration;
using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Filtering;

namespace Fluorite.Strainer.UnitTests.Services.Filtering;

public class FilterTermParserTests
{
    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData(",")]
    public void Parser_ReturnsNoFilterTerms_When_InputIsNullEmptyOrWhitespace(string input)
    {
        // Arrange
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().BeEmpty();
    }

    [Fact]
    public void Parser_ReturnsNoFilterTerms_When_ThereIsNoName()
    {
        // Arrange
        var input = "==SomeValue";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().BeEmpty();
    }

    [Fact]
    public void Parser_ReturnsNoFilterTerms_When_ThereIsOnlyOperator()
    {
        // Arrange
        var input = "==";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().BeEmpty();
    }

    [Fact]
    public void Parser_ReturnsFilterTerm_When_ThereIsOnlyName()
    {
        // Arrange
        var input = "FilterName";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().BeSameAs(input);
        filterTermList[0].Names.Should().BeEquivalentTo(input);
        filterTermList[0].Operator.Should().BeNull();
        filterTermList[0].Values.Should().NotBeNull().And.BeEmpty();
    }

    [Fact]
    public void Parser_ReturnsFilterTerm_When_ThereIsNameWithOperator()
    {
        // Arrange
        var name = "FilterName";
        var filterOperator = "==";
        var input = $"{name}{filterOperator}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().BeSameAs(input);
        filterTermList[0].Names.Should().BeEquivalentTo(name);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().NotBeNull().And.BeEmpty();
    }

    [Fact]
    public void Parser_ReturnsFilterTerm()
    {
        // Arrange
        var name = "FilterName";
        var filterOperator = "==";
        var value = "123";
        var input = $"{name}{filterOperator}{value}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().BeSameAs(input);
        filterTermList[0].Names.Should().BeEquivalentTo(name);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().BeEquivalentTo(value);
    }

    [Fact]
    public void Parser_ReturnsFilterTerm_With_MultipleNames()
    {
        // Arrange
        var name1 = "Foo";
        var name2 = "Bar";
        var filterOperator = "==";
        var value = "123";
        var input = $"{name1}|{name2}{filterOperator}{value}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().BeSameAs(input);
        filterTermList[0].Names.Should().BeEquivalentTo([name1, name2]);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().BeEquivalentTo(value);
    }

    [Fact]
    public void Parser_ReturnsFilterTerm_With_MultipleValues()
    {
        // Arrange
        var name = "FilterName";
        var filterOperator = "==";
        var value1 = "123";
        var value2 = "123";
        var input = $"{name}{filterOperator}{value1}|{value2}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().BeSameAs(input);
        filterTermList[0].Names.Should().BeEquivalentTo(name);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().BeEquivalentTo([value1, value2]);
    }

    [Fact]
    public void Parser_ReturnsFilterTerm_With_MultipleNamesAndValues()
    {
        // Arrange
        var name1 = "Foo";
        var name2 = "Bar";
        var filterOperator = "==";
        var value1 = "123";
        var value2 = "456";
        var input = $"{name1}|{name2}{filterOperator}{value1}|{value2}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().BeSameAs(input);
        filterTermList[0].Names.Should().BeEquivalentTo([name1, name2]);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().BeEquivalentTo([value1, value2]);
    }

    [Fact]
    public void Parser_ReturnsFilterTerm_With_MultipleNamesInParenthesis()
    {
        // Arrange
        var name1 = "Foo";
        var name2 = "Bar";
        var filterOperator = "==";
        var value = "123";
        var input = $"({name1}|{name2}){filterOperator}{value}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms(input);

        // Assert
        filterTermList.Should().HaveCount(1);
        filterTermList[0].Input.Should().Be($"{name1}|{name2}{filterOperator}{value}");
        filterTermList[0].Names.Should().BeEquivalentTo([name1, name2]);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().BeEquivalentTo(value);
    }

    [Fact]
    public void Parser_ReturnsMultipleFilterTerms()
    {
        // Arrange
        var name1 = "Foo";
        var name2 = "Bar";
        var filterOperator = "==";
        var value1 = "123";
        var value2 = "456";
        var input1 = $"{name1}{filterOperator}{value1}";
        var input2 = $"{name2}{filterOperator}{value2}";
        var termParser = BuildFilterTermParser();

        // Act
        var filterTermList = termParser.GetParsedTerms($"{input1},{input2}");

        // Assert
        filterTermList.Should().HaveCount(2);
        filterTermList[0].Input.Should().Be(input1);
        filterTermList[0].Names.Should().BeEquivalentTo(name1);
        filterTermList[0].Operator.Should().NotBeNull();
        filterTermList[0].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[0].Values.Should().BeEquivalentTo(value1);
        filterTermList[1].Input.Should().Be(input2);
        filterTermList[1].Names.Should().BeEquivalentTo(name2);
        filterTermList[1].Operator.Should().NotBeNull();
        filterTermList[1].Operator.Symbol.Should().Be(filterOperator);
        filterTermList[1].Values.Should().BeEquivalentTo(value2);
    }

    private static FilterTermParser BuildFilterTermParser()
    {
        var strainerConfigurationMock = Substitute.For<IStrainerConfiguration>();
        strainerConfigurationMock
            .FilterOperators
            .Returns(FilterOperatorMapper.DefaultOperators);
        var strainerConfigurationProvider = new StrainerConfigurationProvider(strainerConfigurationMock);
        var filterOperatorsProvider = new ConfigurationFilterOperatorsProvider(strainerConfigurationProvider);
        var operatorParser = new FilterOperatorParser(filterOperatorsProvider);
        var namesParser = new FilterTermNamesParser();
        var valuesParser = new FilterTermValuesParser();
        var sectionsParser = new FilterTermSectionsParser(filterOperatorsProvider);

        return new FilterTermParser(operatorParser, namesParser, valuesParser, sectionsParser);
    }
}
