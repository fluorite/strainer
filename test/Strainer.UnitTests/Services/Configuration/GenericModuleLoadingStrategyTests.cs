﻿using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Modules;

namespace Fluorite.Strainer.UnitTests.Services.Configuration;

public class GenericModuleLoadingStrategyTests
{
    private readonly IStrainerModuleBuilderFactory _strainerModuleBuilderFactory = Substitute.For<IStrainerModuleBuilderFactory>();

    private readonly GenericModuleLoadingStrategy _strategy;

    public GenericModuleLoadingStrategyTests()
    {
        _strategy = new GenericModuleLoadingStrategy(_strainerModuleBuilderFactory);
    }

    [Fact]
    public void Should_Throw_ForNonGenericModule()
    {
        // Arrange
        var strainerModule = Substitute.For<IStrainerModule>();

        // Act
        Action act = () => _strategy.Load(strainerModule);

        // Assert
        act.Should().ThrowExactly<ArgumentException>()
            .WithMessage("Strainer module must be generic.*");
    }

    [Fact]
    public void Should_Load_GenericModule()
    {
        // Arrange
        var strainerModule = Substitute.For<IStrainerModule<int>>();

        // Act
        Action act = () => _strategy.Load(strainerModule);

        // Assert
        act.Should().NotThrow();

        strainerModule.Received(1).Load(Arg.Any<IStrainerModuleBuilder<int>>());
        _strainerModuleBuilderFactory.Received(1).Create(typeof(int), strainerModule);
    }
}
