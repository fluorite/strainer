﻿using Fluorite.Strainer.Exceptions;
using Fluorite.Strainer.Models;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Modules;

namespace Fluorite.Strainer.UnitTests.Services.Configuration;

public class StrainerModuleBuilderFactoryTests
{
    private readonly IPropertyInfoProvider _propertyInfoProviderMock = Substitute.For<IPropertyInfoProvider>();
    private readonly IStrainerOptionsProvider _strainerOptionsProviderMock = Substitute.For<IStrainerOptionsProvider>();

    private readonly StrainerModuleBuilderFactory _factory;

    public StrainerModuleBuilderFactoryTests()
    {
        _factory = new StrainerModuleBuilderFactory(
            _propertyInfoProviderMock,
            _strainerOptionsProviderMock);
    }

    [Fact]
    public void Should_CreateModuleBuilder()
    {
        // Arrange
        var moduleTypeParameter = typeof(int);
        var strainerModule = Substitute.For<IStrainerModule>();
        var strainerOptions = new StrainerOptions();

        _strainerOptionsProviderMock.GetStrainerOptions().Returns(strainerOptions);

        // Act
        var result = _factory.Create(moduleTypeParameter, strainerModule);

        // Assert
        result.Should().NotBeNull();
        result.Should().BeOfType<StrainerModuleBuilder<int>>();

        _strainerOptionsProviderMock.Received(1).GetStrainerOptions();
    }

    [Fact]
    public void Should_Throw_ForNotRuntimeType()
    {
        // Arrange
        var moduleTypeParameter = Substitute.For<Type>();
        var strainerModule = Substitute.For<IStrainerModule>();
        var strainerOptions = new StrainerOptions();

        _strainerOptionsProviderMock.GetStrainerOptions().Returns(strainerOptions);

        // Act
        Action act = () => _factory.Create(moduleTypeParameter, strainerModule);

        // Assert
        act.Should().ThrowExactly<StrainerException>()
            .WithMessage($"Unable to create a module builder for module of type {strainerModule.GetType().FullName}.")
            .WithInnerExceptionExactly<ArgumentException>();

        _strainerOptionsProviderMock.Received(1).GetStrainerOptions();
    }
}
