﻿using Fluorite.Strainer.Models.Filtering.Operators;
using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Modules;

namespace Fluorite.Strainer.UnitTests.Services.Configuration;

public class StrainerConfigurationBuilderTests
{
    [Fact]
    public void Should_Throw_ForConflictingCustomFilterOperators()
    {
        // Arrange
        var builder = new StrainerConfigurationBuilder();
        var conflictingSymbol = FilterOperatorSymbols.EqualsSymbol;
        var filterOperator = Substitute.For<IFilterOperator>();
        var filterOperators = new Dictionary<string, IFilterOperator>
        {
            [conflictingSymbol] = filterOperator,
        };
        var module = Substitute.For<IStrainerModule>();
        module
            .FilterOperators
            .Returns(filterOperators);
        var modules = new[]
        {
            module,
        };
        builder.WithCustomFilterOperators(modules);

        // Act
        Action act = () => builder.Build();

        // Assert
        act.Should().ThrowExactly<InvalidOperationException>()
            .WithMessage(
                $"A custom filter operator is conflicting with built-in filter operator on symbol {conflictingSymbol}. " +
                $"Either mark the built-in filter operator to be excluded or remove custom filter operator.");
    }
}
