﻿using Fluorite.Strainer.Exceptions;
using Fluorite.Strainer.Services.Conversion;
using NSubstitute.ExceptionExtensions;

namespace Fluorite.Strainer.UnitTests.Services.Conversion;

public class StringValueConverterTests
{
    private readonly StringValueConverter _converter = new();

    [Fact]
    public void Convert_ThrowsException_WhenConverterThrows()
    {
        // Arrange
        var value = "foo";
        var targetType = typeof(int);
        var typeConverter = Substitute.For<ITypeConverter>();
        var exception = new NotSupportedException();

        typeConverter
            .ConvertFrom(value)
            .Throws(exception);

        // Act
        Action act = () => _converter.Convert(value, targetType, typeConverter);

        // Assert
        var thrownException = act.Should().ThrowExactly<StrainerConversionException>()
            .WithMessage($"Failed to convert value '{value}' to type '{targetType.FullName}'.")
            .Which;

        thrownException.InnerException.Should().BeSameAs(exception);
        thrownException.TargetedType.Should().Be(targetType);
        thrownException.Value.Should().Be(value);
    }

    [Fact]
    public void Convert_Returns_ConvertedValue()
    {
        // Arrange
        var input = "123";
        var targetType = typeof(int);
        var number = 123;
        var typeConverter = Substitute.For<ITypeConverter>();

        typeConverter
            .ConvertFrom(input)
            .Returns(number);

        // Act
        var result = _converter.Convert(input, targetType, typeConverter);

        // Assert
        result.Should().Be(number);
    }
}
