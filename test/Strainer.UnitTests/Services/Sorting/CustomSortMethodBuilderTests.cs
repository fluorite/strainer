﻿using Fluorite.Strainer.Services.Sorting;
using System.Linq.Expressions;

namespace Fluorite.Strainer.UnitTests.Services.Sorting;

public class CustomSortMethodBuilderTests
{
    [Fact]
    public void Should_Build_CustomSortMethod_UsingDirectExpression()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Post, object>> expression1 = x => x.Author != null;
        var builder = new CustomSortMethodBuilder<Post>()
            .HasName(name)
            .HasFunction(expression1);

        // Act
        var result = builder.Build();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().BeSameAs(expression1);
        result.Name.Should().Be(name);
        result.ExpressionProvider.Should().BeNull();
    }

    [Fact]
    public void Should_Build_CustomSortMethod_UsingProviderExpression()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Post, object>> expression1 = x => x.Author != null;
        var builder = new CustomSortMethodBuilder<Post>()
            .HasName(name)
            .HasFunction(_ => expression1);

        // Act
        var result = builder.Build();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().BeNull();
        result.Name.Should().Be(name);
        result.ExpressionProvider.Should().NotBeNull();
        result.ExpressionProvider.Invoke(null).Should().BeSameAs(expression1);
    }

    [Fact]
    public void Should_Build_CustomSortMethod_UsingDirectExpression_ErasingPreviousExpression()
    {
        // Arrange
        var name = "foo";
        Expression<Func<Post, object>> expression1 = x => x.Author != null;
        Expression<Func<Post, object>> expression2 = x => x.Name != null;
        var builder = new CustomSortMethodBuilder<Post>()
            .HasName(name)
            .HasFunction(expression1)
            .HasFunction(expression2);

        // Act
        var result = builder.Build();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().BeSameAs(expression2);
        result.Name.Should().BeSameAs(name);
        result.ExpressionProvider.Should().BeNull();
    }

    private class Post
    {
        public string Author { get; set; }

        public string Name { get; set; }
    }
}
