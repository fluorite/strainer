﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Models.Sorting.Terms;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Sorting;
using NSubstitute.ReceivedExtensions;
using System.Reflection;

namespace Fluorite.Strainer.UnitTests.Services.Sorting;

public class SortExpressionProviderTests
{
    private readonly IMetadataFacade _metadataProvidersFacadeMock = Substitute.For<IMetadataFacade>();
    private readonly IStrainerOptionsProvider _strainerOptionsProviderMock = Substitute.For<IStrainerOptionsProvider>();

    private readonly SortExpressionProvider _provider;

    public SortExpressionProviderTests()
    {
        _provider = new SortExpressionProvider(
            _metadataProvidersFacadeMock,
            _strainerOptionsProviderMock);
    }

    [Fact]
    public void Provider_Returns_NullDefaultExpression_When_MetadataForTypeIsNull()
    {
        // Arrange
        _metadataProvidersFacadeMock
            .GetDefaultMetadata<Comment>()
            .ReturnsNull();

        // Act
        var result = _provider.GetDefaultExpression<Comment>();

        // Assert
        result.Should().BeNull();

        _metadataProvidersFacadeMock
            .Received(1)
            .GetDefaultMetadata<Comment>();
    }

    [Fact]
    public void Provider_Returns_DefaultExpression()
    {
        // Arrange
        var name = nameof(Comment.Id);
        var propertyInfo = typeof(Comment).GetProperty(name);
        var sortingWay = SortingWay.Descending;
        var defaultMetadata = Substitute.For<IPropertyMetadata>();
        defaultMetadata.PropertyInfo.Returns(propertyInfo);
        defaultMetadata.DisplayName.ReturnsNull();
        defaultMetadata.IsDefaultSorting.Returns(false);
        defaultMetadata.DefaultSortingWay.ReturnsNull();
        defaultMetadata.Name.Returns(name);

        var strainerOptions = new StrainerOptions
        {
            DefaultSortingWay = sortingWay,
        };

        _metadataProvidersFacadeMock
            .GetDefaultMetadata<Comment>()
            .Returns(defaultMetadata);
        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);

        // Act
        var result = _provider.GetDefaultExpression<Comment>();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().NotBeNull();
        result.IsDefault.Should().BeFalse();
        result.IsDescending.Should().Be(sortingWay == SortingWay.Descending);
        result.IsSubsequent.Should().BeFalse();

        _metadataProvidersFacadeMock
            .Received(1)
            .GetDefaultMetadata<Comment>();
    }

    [Fact]
    public void Provider_Returns_DefaultExpression_WithSortingWayFromProperty()
    {
        // Arrange
        var name = nameof(Comment.Id);
        var propertyInfo = typeof(Comment).GetProperty(name);
        var sortingWay = SortingWay.Ascending;
        var defaultMetadata = Substitute.For<IPropertyMetadata>();
        defaultMetadata.PropertyInfo.Returns(propertyInfo);
        defaultMetadata.DisplayName.ReturnsNull();
        defaultMetadata.IsDefaultSorting.Returns(true);
        defaultMetadata.DefaultSortingWay.Returns(sortingWay);
        defaultMetadata.Name.Returns(name);

        _metadataProvidersFacadeMock
            .GetDefaultMetadata<Comment>()
            .Returns(defaultMetadata);

        // Act
        var result = _provider.GetDefaultExpression<Comment>();

        // Assert
        result.Should().NotBeNull();
        result.Expression.Should().NotBeNull();
        result.IsDefault.Should().BeTrue();
        result.IsDescending.Should().Be(sortingWay == SortingWay.Descending);
        result.IsSubsequent.Should().BeFalse();

        _metadataProvidersFacadeMock
            .Received(1)
            .GetDefaultMetadata<Comment>();
        _strainerOptionsProviderMock
            .DidNotReceive()
            .GetStrainerOptions();
    }

    [Fact]
    public void Provider_Returns_ListOfSortExpressions()
    {
        // Arrange
        var propertyInfo = typeof(Comment).GetProperty(nameof(Comment.Text));
        var propertyMetadata = new PropertyMetadata(propertyInfo.Name, propertyInfo)
        {
            IsSortable = true,
        };
        var sortTerm = new SortTerm(propertyInfo.Name)
        {
            IsDescending = false,
        };
        var sortTerms = new Dictionary<IPropertyMetadata, ISortTerm>
        {
            { propertyMetadata, sortTerm },
        };

        _metadataProvidersFacadeMock
            .GetMetadata<Comment>(true, false, sortTerm.Name)
            .Returns(propertyMetadata);

        // Act
        var sortExpressions = _provider.GetExpressions<Comment>(sortTerms).ToList();

        // Assert
        sortExpressions.Should().NotBeEmpty();
        sortExpressions[0].IsDefault.Should().BeFalse();
        sortExpressions[0].IsDescending.Should().BeFalse();
        sortExpressions[0].IsSubsequent.Should().BeFalse();
    }

    [Fact]
    public void Provider_Returns_ListOfSortExpressions_For_NestedProperty()
    {
        // Arrange
        var name = $"{nameof(Post.TopComment)}.{nameof(Comment.Id)}";
        var sortTerm = new SortTerm(name)
        {
            Input = $"-{nameof(Post.TopComment)}.{nameof(Comment.Id)}",
            IsDescending = true,
        };
        var propertyMetadata = Substitute.For<IPropertyMetadata>();
        propertyMetadata.PropertyInfo.Returns(typeof(Comment).GetProperty(nameof(Comment.Id)));
        propertyMetadata.Name.Returns(sortTerm.Name);
        propertyMetadata.IsSortable.Returns(true);
        var sortTerms = new Dictionary<IPropertyMetadata, ISortTerm>
        {
            { propertyMetadata, sortTerm },
        };

        _metadataProvidersFacadeMock
            .GetMetadata<Post>(true, false, sortTerm.Name)
            .Returns(propertyMetadata);

        // Act
        var sortExpressions = _provider.GetExpressions<Post>(sortTerms);

        // Assert
        sortExpressions.Should().NotBeEmpty();
        sortExpressions[0].IsDefault.Should().BeFalse();
        sortExpressions[0].IsDescending.Should().BeTrue();
        sortExpressions[0].IsSubsequent.Should().BeFalse();
    }

    [Fact]
    public void Provider_Returns_ListOfSortExpressions_For_SubsequentSorting()
    {
        // Arrange
        var termsList = new List<ISortTerm>
        {
            new SortTerm(nameof(Comment.Text))
            {
                Input = $"-{nameof(Comment.Text)},{nameof(Comment.Id)}",
                IsDescending = true,
            },
            new SortTerm(nameof(Comment.Id))
            {
                Input = $"-{nameof(Comment.Text)},{nameof(Comment.Id)}",
                IsDescending = false,
            },
        };
        var properties = new PropertyInfo[]
        {
            typeof(Comment).GetProperty(nameof(Comment.Text)),
            typeof(Comment).GetProperty(nameof(Comment.Id)),
        };
        var propertyMetadatas = new[]
        {
            new PropertyMetadata(nameof(Comment.Text), properties[0])
            {
                IsSortable = true,
            },
            new PropertyMetadata(nameof(Comment.Id), properties[1])
            {
                IsSortable = true,
            },
        };
        var sortTerms = new Dictionary<IPropertyMetadata, ISortTerm>
        {
            { propertyMetadatas[0], termsList[0] },
            { propertyMetadatas[1], termsList[1] },
        };

        _metadataProvidersFacadeMock
            .GetMetadata<Comment>(true, false, termsList[0].Name)
            .Returns(propertyMetadatas[0]);
        _metadataProvidersFacadeMock
            .GetMetadata<Comment>(true, false, termsList[1].Name)
            .Returns(propertyMetadatas[1]);

        // Act
        var sortExpressions = _provider.GetExpressions<Comment>(sortTerms).ToList();

        // Assert
        sortExpressions.Should().HaveCount(2);
        sortExpressions[0].IsDefault.Should().BeFalse();
        sortExpressions[0].IsDescending.Should().BeTrue();
        sortExpressions[0].IsSubsequent.Should().BeFalse();
        sortExpressions[1].IsDefault.Should().BeFalse();
        sortExpressions[1].IsDescending.Should().BeFalse();
        sortExpressions[1].IsSubsequent.Should().BeTrue();
    }

    private class Comment
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }

    private class Post
    {
        public Comment TopComment { get; set; }
    }
}
