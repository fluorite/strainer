﻿using Fluorite.Strainer.Exceptions;
using Fluorite.Strainer.Models;
using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Models.Sorting.Terms;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Sorting;
using NSubstitute.ReceivedExtensions;
using System.Linq.Expressions;

namespace Fluorite.Strainer.UnitTests.Services.Sorting;

public class SortingApplierTests
{
    private readonly ICustomSortingExpressionProvider _customSortingExpressionProviderMock = Substitute.For<ICustomSortingExpressionProvider>();
    private readonly ISortExpressionProvider _sortExpressionProviderMock = Substitute.For<ISortExpressionProvider>();
    private readonly IMetadataFacade _metadataFacadeMock = Substitute.For<IMetadataFacade>();
    private readonly IStrainerOptionsProvider _strainerOptionsProviderMock = Substitute.For<IStrainerOptionsProvider>();

    private readonly SortingApplier _applier;

    public SortingApplierTests()
    {
        _applier = new SortingApplier(
            _customSortingExpressionProviderMock,
            _sortExpressionProviderMock,
            _metadataFacadeMock,
            _strainerOptionsProviderMock);
    }

    [Fact]
    public void Should_DoNotApplySorting_WhenTermsCollectionIsEmpty()
    {
        // Arrange
        var sortTerms = new List<ISortTerm>();
        var source = Array.Empty<Post>().AsQueryable();
        var strainerOptions = new StrainerOptions();

        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);

        // Act
        var result = _applier.TryApplySorting(sortTerms, source, out var sortedSource);

        // Assert
        result.Should().BeFalse();
        sortedSource.Should().BeNull();

        _strainerOptionsProviderMock
            .Received(1)
            .GetStrainerOptions();
    }

    [Fact]
    public void Should_Throw_WhenPropertyMetadataIsNotFound_AndExceptionThrowingIsEnabled()
    {
        // Arrange
        var sortTerm = Substitute.For<ISortTerm>();
        sortTerm.Name.Returns("foo");
        var sortTerms = new List<ISortTerm>
        {
            sortTerm,
        };
        var source = Array.Empty<Post>().AsQueryable();
        var strainerOptions = new StrainerOptions
        {
            ThrowExceptions = true,
        };

        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);
        _metadataFacadeMock
            .GetMetadata<Post>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name)
            .ReturnsNull();
        _customSortingExpressionProviderMock
            .TryGetCustomExpression<Post>(sortTerm, isSubsequent: false, out var sortExpression)
            .Returns(false);

        // Act
        Action act = () => _applier.TryApplySorting(sortTerms, source, out var sortedSource);

        // Assert
        act.Should().Throw<StrainerSortNotFoundException>()
            .WithMessage($"Property or custom sorting method '{sortTerm.Name}' was not found.");

        _strainerOptionsProviderMock
            .Received(1)
            .GetStrainerOptions();
        _metadataFacadeMock
            .Received(1)
            .GetMetadata<Post>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name);
        _customSortingExpressionProviderMock
            .Received(1)
            .TryGetCustomExpression(sortTerm, isSubsequent: false, out sortExpression);
    }

    [Fact]
    public void Should_DoNotApplySorting_WhenPropertyMetadataIsNotFound_AndExceptionThrowingIsDisabled()
    {
        // Arrange
        var sortTerm = Substitute.For<ISortTerm>();
        sortTerm.Name.Returns("foo");
        var sortTerms = new List<ISortTerm>
        {
            sortTerm,
        };
        var source = Array.Empty<Post>().AsQueryable();
        var strainerOptions = new StrainerOptions();

        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);
        _metadataFacadeMock
            .GetMetadata<Post>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name)
            .ReturnsNull();
        _customSortingExpressionProviderMock
            .TryGetCustomExpression<Post>(sortTerm, isSubsequent: false, out var sortExpression)
            .Returns(false);

        // Act
        var result = _applier.TryApplySorting(sortTerms, source, out var sortedSource);

        // Assert
        result.Should().BeFalse();
        sortedSource.Should().BeNull();

        _strainerOptionsProviderMock
            .Received(1)
            .GetStrainerOptions();
        _metadataFacadeMock
            .Received(1)
            .GetMetadata<Post>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name);
        _customSortingExpressionProviderMock
            .Received(1)
            .TryGetCustomExpression(sortTerm, isSubsequent: false, out sortExpression);
    }

    [Fact]
    public void Should_ApplySorting_FromCustomSortingMethod()
    {
        // Arrange
        var sortTerm = Substitute.For<ISortTerm>();
        sortTerm.Name.Returns(nameof(string.Length));
        var sortTerms = new List<ISortTerm>
        {
            sortTerm,
        };
        var source = new[] { "foo", "foo bar" }.AsQueryable();
        var strainerOptions = new StrainerOptions();
        Expression<Func<string, object>> expression = x => x.Length;
        var sortExpression = Substitute.For<ISortExpression<string>>();
        sortExpression.IsSubsequent.Returns(false);
        sortExpression.IsDescending.Returns(true);
        sortExpression.Expression.Returns(expression);

        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);
        _metadataFacadeMock
            .GetMetadata<string>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name)
            .ReturnsNull();
        _customSortingExpressionProviderMock
            .TryGetCustomExpression<string>(sortTerm, isSubsequent: false, out _)
            .Returns(x =>
            {
                x[2] = sortExpression;

                return true;
            });

        // Act
        var result = _applier.TryApplySorting(sortTerms, source, out var sortedSource);

        // Assert
        result.Should().BeTrue();
        sortedSource.Should().NotBeNullOrEmpty();
        sortedSource.Should().BeInDescendingOrder(expression);

        _strainerOptionsProviderMock
            .Received(1)
            .GetStrainerOptions();
        _metadataFacadeMock
            .Received(1)
            .GetMetadata<string>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name);
        _customSortingExpressionProviderMock
            .Received(1)
            .TryGetCustomExpression<string>(sortTerm, isSubsequent: false, out _);
    }

    [Fact]
    public void Should_ApplySorting_FromPropertyMetadata()
    {
        // Arrange
        var sortTerm = Substitute.For<ISortTerm>();
        sortTerm.Name.Returns(nameof(string.Length));
        var sortTerms = new List<ISortTerm>
        {
            sortTerm,
        };
        var source = new[] { "foo", "foo bar" }.AsQueryable();
        var strainerOptions = new StrainerOptions();
        var metadata = Substitute.For<IPropertyMetadata>();
        Expression<Func<string, object>> expression = x => x.Length;
        var sortExpression = Substitute.For<ISortExpression<string>>();
        sortExpression.IsSubsequent.Returns(false);
        sortExpression.IsDescending.Returns(true);
        sortExpression.Expression.Returns(expression);

        _strainerOptionsProviderMock
            .GetStrainerOptions()
            .Returns(strainerOptions);
        _metadataFacadeMock
            .GetMetadata<string>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name)
            .Returns(metadata);
        _sortExpressionProviderMock
            .GetExpression<string>(metadata, sortTerm, isSubsequent: false)
            .Returns(sortExpression);

        // Act
        var result = _applier.TryApplySorting(sortTerms, source, out var sortedSource);

        // Assert
        result.Should().BeTrue();
        sortedSource.Should().NotBeNullOrEmpty();
        sortedSource.Should().BeInDescendingOrder(expression);

        _strainerOptionsProviderMock
            .Received(1)
            .GetStrainerOptions();
        _metadataFacadeMock
            .Received(1)
            .GetMetadata<string>(isSortableRequired: true, isFilterableRequired: false, sortTerm.Name);
        _sortExpressionProviderMock
            .Received(1)
            .GetExpression<string>(metadata, sortTerm, isSubsequent: false);
        _customSortingExpressionProviderMock
            .DidNotReceive()
            .TryGetCustomExpression<string>(sortTerm, isSubsequent: false, out _);
    }

    private class Post
    {
    }
}
