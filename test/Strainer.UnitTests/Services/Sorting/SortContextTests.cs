﻿using Fluorite.Strainer.Services.Sorting;
using Fluorite.Strainer.Services.Validation;

namespace Fluorite.Strainer.UnitTests.Services.Sorting;

public class SortContextTests
{
    [Fact]
    public void Should_Create_SortContext()
    {
        // Arrange
        var sortExpressionProvider = Substitute.For<ISortExpressionProvider>();
        var expressionValidator = Substitute.For<ISortExpressionValidator>();
        var sortingWayFormatter = Substitute.For<ISortingWayFormatter>();
        var sortTermParser = Substitute.For<ISortTermParser>();
        var sortTermValueParser = Substitute.For<ISortTermValueParser>();

        // Act
        var result = new SortingContext(
            sortExpressionProvider,
            expressionValidator,
            sortingWayFormatter,
            sortTermParser,
            sortTermValueParser);

        // Assert
        result.ExpressionProvider.Should().BeSameAs(sortExpressionProvider);
        result.ExpressionValidator.Should().BeSameAs(expressionValidator);
        result.Formatter.Should().BeSameAs(sortingWayFormatter);
        result.TermParser.Should().BeSameAs(sortTermParser);
        result.TermValueParser.Should().BeSameAs(sortTermValueParser);
    }
}
