﻿global using FluentAssertions;
global using NSubstitute;
global using NSubstitute.ReturnsExtensions;
global using System;
global using System.Collections.Generic;
global using System.Linq;
global using Xunit;
