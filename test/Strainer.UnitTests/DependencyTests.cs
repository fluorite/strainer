﻿namespace Fluorite.Strainer.UnitTests;

public class DependencyTests
{
    private const string Explanation =
        "Starting from Version 8.0 Fluent Assertions uses new non-commercial license. " +
        "Even though this is an OSS project, so non-commercial license does not change " +
        "anything especially when not shipped along the library. " +
        "But still, this rugpull was pretty nasty, so let's stay on 7.x.x for as long as we can. " +
        "See https://github.com/fluentassertions/fluentassertions/pull/2943 for more.";

    [Fact]
    public void Should_NotUse_FluentAssertions_Version8()
    {
        // Act
        var fluentAssertionsAssembly = AppDomain.CurrentDomain.Load("FluentAssertions");

        // Assert
        fluentAssertionsAssembly.Should().NotBeNull();
        fluentAssertionsAssembly.GetName().Should().NotBeNull();
        fluentAssertionsAssembly.GetName().Version.Should().NotBeNull();
        fluentAssertionsAssembly.GetName().Version.Should().BeLessThan(new Version(8, 0), because: Explanation);
    }
}
