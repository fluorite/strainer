﻿using Fluorite.Strainer.AspNetCore.Services;
using Fluorite.Strainer.Models;
using Fluorite.Strainer.Services;
using Fluorite.Strainer.Services.Configuration;
using Fluorite.Strainer.Services.Conversion;
using Fluorite.Strainer.Services.Filtering;
using Fluorite.Strainer.Services.Filtering.Steps;
using Fluorite.Strainer.Services.Linq;
using Fluorite.Strainer.Services.Metadata;
using Fluorite.Strainer.Services.Metadata.Attributes;
using Fluorite.Strainer.Services.Metadata.FluentApi;
using Fluorite.Strainer.Services.Modules;
using Fluorite.Strainer.Services.Pagination;
using Fluorite.Strainer.Services.Pipelines;
using Fluorite.Strainer.Services.Sorting;
using Fluorite.Strainer.Services.Validation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Reflection;

namespace Fluorite.Extensions.DependencyInjection;

/// <summary>
/// Provides extensions for adding Strainer services to <see cref="IServiceCollection"/>.
/// </summary>
public static class StrainerServiceCollectionExtensions
{
    /// <summary>
    /// The default service lifetime for Strainer services.
    /// </summary>
    public const ServiceLifetime DefaultServiceLifetime = ServiceLifetime.Scoped;

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);

        return services.AddStrainer(new List<Type>(), serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a collection of assemblies containing Strainer module types.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="assembliesToScan">
    /// Assemblies that will be scanned in search for non-abstract classes
    /// deriving from <see cref="StrainerModule"/>. Matching classes will
    /// be added to Strainer as configuration modules.
    /// <para />
    /// Referenced assemblies will not be included.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="assembliesToScan"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        Assembly[] assembliesToScan,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(assembliesToScan);

        var moduleTypes = GetModuleTypesFromAssemblies(assembliesToScan);

        services.TryAddSingleton<IMetadataAssemblySourceProvider>(new AssemblySourceProvider(assembliesToScan));

        return services.AddStrainer(moduleTypes, serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a collection of Strainer module types.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="moduleTypes">
    /// The types of Strainer modules.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="moduleTypes"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        IReadOnlyCollection<Type> moduleTypes,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(moduleTypes);

        RegisterStrainerServices(services, moduleTypes, serviceLifetime);

        return services;
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a configuration.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configuration">
    /// A configuration used to bind against <see cref="StrainerOptions"/>.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configuration"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        IConfiguration configuration,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(configuration);

        return services.AddStrainer(configuration, new List<Type>(), serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a configuration and a collection of Strainer module types.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configuration">
    /// A configuration used to bind against <see cref="StrainerOptions"/>.
    /// </param>
    /// <param name="moduleTypes">
    /// The types of Strainer modules.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configuration"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="moduleTypes"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        IConfiguration configuration,
        IReadOnlyCollection<Type> moduleTypes,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(configuration);
        Guard.Against.Null(moduleTypes);

        services.Configure<StrainerOptions>(configuration);

        return services.AddStrainer(moduleTypes, serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a configuration and a collection of assemblies containing
    /// Strainer module types.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configuration">
    /// A configuration used to bind against <see cref="StrainerOptions"/>.
    /// </param>
    /// <param name="assembliesToScan">
    /// Assemblies that will be scanned in search for non-abstract classes
    /// deriving from <see cref="StrainerModule"/>. Matching classes will
    /// be added to Strainer as configuration modules.
    /// <para />
    /// Referenced assemblies will not be included.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configuration"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="assembliesToScan"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        IConfiguration configuration,
        Assembly[] assembliesToScan,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(configuration);
        Guard.Against.Null(assembliesToScan);

        services.Configure<StrainerOptions>(configuration);
        services.TryAddSingleton<IMetadataAssemblySourceProvider>(new AssemblySourceProvider(assembliesToScan));

        return services.AddStrainer(assembliesToScan, serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a configuration action.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configure">
    /// An action used to configure <see cref="StrainerOptions"/>.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configure"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        Action<StrainerOptions> configure,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(configure);

        return services.AddStrainer(configure, new List<Type>(), serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a configuration action and a collection of Strainer module types.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configure">
    /// An action used to configure <see cref="StrainerOptions"/>.
    /// </param>
    /// <param name="moduleTypes">
    /// The types of Strainer modules.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configure"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="moduleTypes"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        Action<StrainerOptions> configure,
        IReadOnlyCollection<Type> moduleTypes,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(configure);
        Guard.Against.Null(moduleTypes);

        services.AddOptions<StrainerOptions>().Configure(configure);

        return services.AddStrainer(moduleTypes, serviceLifetime);
    }

    /// <summary>
    /// Adds Strainer services to the <see cref="IServiceCollection"/>
    /// with a configuration action and a collection of assemblies containing
    /// Strainer module types.
    /// </summary>
    /// <param name="services">
    /// Current instance of <see cref="IServiceCollection"/>.
    /// </param>
    /// <param name="configure">
    /// An action used to configure <see cref="StrainerOptions"/>.
    /// </param>
    /// <param name="assembliesToScan">
    /// Assemblies that will be scanned in search for non-abstract classes
    /// deriving from <see cref="StrainerModule"/>. Matching classes will
    /// be added to Strainer as configuration modules.
    /// <para />
    /// Referenced assemblies will not be included.
    /// </param>
    /// <param name="serviceLifetime">
    /// The service lifetime for Strainer services.
    /// </param>
    /// <returns>
    /// An instance of <see cref="IServiceCollection"/> with added
    /// Strainer services, so additional calls can be chained.
    /// </returns>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="services"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="configure"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="assembliesToScan"/> is <see langword="null"/>.
    /// </exception>
    public static IServiceCollection AddStrainer(
        this IServiceCollection services,
        Action<StrainerOptions> configure,
        Assembly[] assembliesToScan,
        ServiceLifetime serviceLifetime = DefaultServiceLifetime)
    {
        Guard.Against.Null(services);
        Guard.Against.Null(configure);
        Guard.Against.Null(assembliesToScan);

        services.AddOptions<StrainerOptions>().Configure(configure);
        services.TryAddSingleton<IMetadataAssemblySourceProvider>(new AssemblySourceProvider(assembliesToScan));

        return services.AddStrainer(assembliesToScan, serviceLifetime);
    }

    private static void RegisterStrainerServices(
        IServiceCollection services,
        IReadOnlyCollection<Type> moduleTypes,
        ServiceLifetime serviceLifetime)
    {
        services.AddOptions<StrainerOptions>();

        if (serviceLifetime == ServiceLifetime.Singleton)
        {
            services.TryAdd<IStrainerOptionsProvider, AspNetCoreSingletonStrainerOptionsProvider>(serviceLifetime);
        }
        else
        {
            services.TryAdd<IStrainerOptionsProvider, AspNetCoreStrainerOptionsProvider>(serviceLifetime);
        }

        services.TryAdd<IQueryableEvaluator, QueryableEvaluator>(serviceLifetime);
        services.TryAdd<IFilterExpressionProvider, FilterExpressionProvider>(serviceLifetime);
        services.TryAdd<IFilterOperatorParser, FilterOperatorParser>(serviceLifetime);
        services.TryAdd<IFilterOperatorValidator, FilterOperatorValidator>(serviceLifetime);
        services.TryAdd<IFilterTermNamesParser, FilterTermNamesParser>(serviceLifetime);
        services.TryAdd<IFilterTermValuesParser, FilterTermValuesParser>(serviceLifetime);
        services.TryAdd<IFilterTermSectionsParser, FilterTermSectionsParser>(serviceLifetime);
        services.TryAdd<IFilterTermParser, FilterTermParser>(serviceLifetime);
        services.TryAdd<ICustomFilteringExpressionProvider, CustomFilteringExpressionProvider>(serviceLifetime);
        services.TryAdd<IFilterExpressionWorkflowBuilder, FilterExpressionWorkflowBuilder>(serviceLifetime);
        services.TryAdd<IConvertPropertyValueToStringStep, ConvertPropertyValueToStringStep>(serviceLifetime);
        services.TryAdd<IConvertFilterValueToStringStep, ConvertFilterValueToStringStep>(serviceLifetime);
        services.TryAdd<IChangeTypeOfFilterValueStep, ChangeTypeOfFilterValueStep>(serviceLifetime);
        services.TryAdd<IApplyConsantClosureToFilterValueStep, ApplyConsantClosureToFilterValueStep>(serviceLifetime);
        services.TryAdd<IMitigateCaseInsensitivityStep, MitigateCaseInsensitivityStep>(serviceLifetime);
        services.TryAdd<IApplyFilterOperatorStep, ApplyFilterOperatorStep>(serviceLifetime);
        services.TryAdd<IFilterContext, FilterContext>(serviceLifetime);

        services.TryAdd<ISortExpressionProvider, SortExpressionProvider>(serviceLifetime);
        services.TryAdd<ISortExpressionValidator, SortExpressionValidator>(serviceLifetime);
        services.TryAdd<ISortingWayFormatter, DescendingPrefixSortingWayFormatter>(serviceLifetime);
        services.TryAdd<ISortTermValueParser, SortTermValueParser>(serviceLifetime);
        services.TryAdd<ISortTermParser, SortTermParser>(serviceLifetime);
        services.TryAdd<ISortingApplier, SortingApplier>(serviceLifetime);
        services.TryAdd<ICustomSortingExpressionProvider, CustomSortingExpressionProvider>(serviceLifetime);
        services.TryAdd<ISortingContext, SortingContext>(serviceLifetime);
        services.TryAdd<IPipelineContext, PipelineContext>(serviceLifetime);

        services.TryAdd<IPageNumberEvaluator, PageNumberEvaluator>(serviceLifetime);
        services.TryAdd<IPageSizeEvaluator, PageSizeEvaluator>(serviceLifetime);

        services.TryAdd<IStrainerPipelineBuilderFactory, StrainerPipelineBuilderFactory>(serviceLifetime);
        services.TryAdd<IFilterPipelineOperation, FilterPipelineOperation>(serviceLifetime);
        services.TryAdd<ISortPipelineOperation, SortPipelineOperation>(serviceLifetime);
        services.TryAdd<IPaginatePipelineOperation, PaginatePipelineOperation>(serviceLifetime);

        services.TryAddSingleton<IMetadataAssemblySourceProvider, AppDomainAssemblySourceProvider>();
        services.TryAdd<IMetadataSourceTypeProvider, MetadataSourceTypeProvider>(serviceLifetime);

        services.TryAdd<IAttributePropertyMetadataBuilder, AttributePropertyMetadataBuilder>(serviceLifetime);
        services.TryAdd<IAttributeMetadataRetriever, AttributeMetadataRetriever>(serviceLifetime);
        services.TryAdd<IAttributeCriteriaChecker, AttributeCriteriaChecker>(serviceLifetime);
        services.TryAdd<IStrainerAttributeProvider, StrainerAttributeProvider>(serviceLifetime);
        services.TryAdd<IPropertyMetadataDictionaryProvider, PropertyMetadataDictionaryProvider>(serviceLifetime);
        services.TryAdd<IPropertyInfoProvider, PropertyInfoProvider>(serviceLifetime);
        services.TryAdd<IMetadataSourceChecker, MetadataSourceChecker>(serviceLifetime);
        services.TryAdd<IFluentApiPropertyMetadataBuilder, FluentApiPropertyMetadataBuilder>(serviceLifetime);
        services.TryAddEnumerable<IMetadataProvider, FluentApiMetadataProvider>(serviceLifetime);
        services.TryAddEnumerable<IMetadataProvider, AttributeMetadataProvider>(serviceLifetime);
        services.TryAdd<IMetadataFacade, MetadataFacade>(serviceLifetime);

        services.TryAdd<ITypeConverterProvider, TypeConverterProvider>(serviceLifetime);
        services.TryAdd<IStringValueConverter, StringValueConverter>(serviceLifetime);
        services.TryAdd<ITypeChanger, TypeChanger>(serviceLifetime);

        services.TryAdd<IStrainerConfigurationFactory, StrainerConfigurationFactory>(serviceLifetime);
        services.TryAdd<IStrainerModuleFactory, StrainerModuleFactory>(serviceLifetime);
        services.TryAdd<IStrainerModuleLoader, StrainerModuleLoader>(serviceLifetime);
        services.TryAdd<IModuleLoadingStrategySelector, ModuleLoadingStrategySelector>(serviceLifetime);
        services.TryAdd<IPlainModuleLoadingStrategy, PlainModuleLoadingStrategy>(serviceLifetime);
        services.TryAdd<IGenericModuleLoadingStrategy, GenericModuleLoadingStrategy>(serviceLifetime);
        services.TryAdd<IStrainerModuleBuilderFactory, StrainerModuleBuilderFactory>(serviceLifetime);
        services.TryAdd<IStrainerModuleTypeValidator, StrainerModuleTypeValidator>(serviceLifetime);
        services.TryAdd<IStrainerConfigurationValidator, StrainerConfigurationValidator>(serviceLifetime);
        services.TryAdd<IConfigurationCustomMethodsProvider, ConfigurationCustomMethodsProvider>(serviceLifetime);
        services.TryAdd<IConfigurationFilterOperatorsProvider, ConfigurationFilterOperatorsProvider>(serviceLifetime);
        services.TryAdd<IConfigurationMetadataProvider, ConfigurationMetadataProvider>(serviceLifetime);

        services.TryAdd<IStrainerContext, StrainerContext>(serviceLifetime);
        services.TryAdd<IStrainerProcessor, StrainerProcessor>(serviceLifetime);

        services.TryAddSingleton<IStrainerConfigurationProvider>(serviceProvider =>
        {
            using var scope = serviceProvider.CreateScope();
            var configurationFactory = scope.ServiceProvider.GetRequiredService<IStrainerConfigurationFactory>();
            var configurationValidator = scope.ServiceProvider.GetRequiredService<IStrainerConfigurationValidator>();

            try
            {
                var strainerConfiguration = configurationFactory.Create(moduleTypes);
                configurationValidator.Validate(strainerConfiguration);

                return new StrainerConfigurationProvider(strainerConfiguration);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Unable to add configuration from Strainer modules.", ex);
            }
        });
    }

    private static void TryAdd<TServiceType, TImplementationType>(
        this IServiceCollection services,
        ServiceLifetime serviceLifetime)
            where TImplementationType : TServiceType
    {
        services.TryAdd(new ServiceDescriptor(typeof(TServiceType), typeof(TImplementationType), serviceLifetime));
    }

    private static void TryAddEnumerable<TServiceType, TImplementationType>(
        this IServiceCollection services,
        ServiceLifetime serviceLifetime)
            where TImplementationType : TServiceType
    {
        services.TryAddEnumerable(new ServiceDescriptor(typeof(TServiceType), typeof(TImplementationType), serviceLifetime));
    }

    private static List<Type> GetModuleTypesFromAssemblies(IReadOnlyCollection<Assembly> assemblies)
    {
        return assemblies
            .Distinct()
            .SelectMany(a => a.GetTypes())
            .SelectMany(type => new[] { type }.Union(type.GetNestedTypes()))
            .Where(type => !type.IsAbstract && typeof(IStrainerModule).IsAssignableFrom(type))
            .ToList();
    }
}
