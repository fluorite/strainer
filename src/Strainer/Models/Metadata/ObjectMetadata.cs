﻿using Fluorite.Strainer.Models.Sorting;
using System.Reflection;

namespace Fluorite.Strainer.Models.Metadata;

/// <summary>
/// Represents filtering and sorting metadata for a class or struct,
/// setting default values for all its properties.
/// </summary>
public class ObjectMetadata : IObjectMetadata
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ObjectMetadata"/> class.
    /// </summary>
    public ObjectMetadata(
        string defaultSortingPropertyName,
        PropertyInfo defaultSortingPropertyInfo,
        SortingWay? defaultSortingWay)
    {
        DefaultSortingPropertyName = Guard.Against.NullOrWhiteSpace(defaultSortingPropertyName);
        DefaultSortingPropertyInfo = Guard.Against.Null(defaultSortingPropertyInfo);
        DefaultSortingWay = defaultSortingWay;
    }

    /// <inheritdoc/>
    public string DefaultSortingPropertyName { get; }

    /// <inheritdoc/>
    public SortingWay? DefaultSortingWay { get; }

    /// <summary>
    /// Gets or sets a value indicating whether related
    /// object is marked as filterable.
    /// </summary>
    public bool IsFilterable { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether related
    /// object is marked as filterable.
    /// </summary>
    public bool IsSortable { get; set; }

    /// <inheritdoc/>
    public PropertyInfo DefaultSortingPropertyInfo { get; }
}
