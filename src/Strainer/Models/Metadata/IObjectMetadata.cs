﻿using Fluorite.Strainer.Models.Sorting;
using System.Reflection;

namespace Fluorite.Strainer.Models.Metadata;

/// <summary>
/// Represents filtering and sorting metadata for a class or struct,
/// setting default values for all its properties.
/// </summary>
public interface IObjectMetadata
{
    /// <summary>
    /// Gets a property name that will be used as a default (fallback) property
    /// when no sorting information is available but sorting needs to be applied.
    /// </summary>
    string DefaultSortingPropertyName { get; }

    /// <summary>
    /// Gets default sorting way used when applying sorting using default sorting property.
    /// </summary>
    SortingWay? DefaultSortingWay { get; }

    /// <summary>
    /// Gets a value indicating whether related
    /// object is marked as filterable.
    /// </summary>
    bool IsFilterable { get; }

    /// <summary>
    /// Gets a value indicating whether related
    /// object is marked as filterable.
    /// <para/>
    /// Defaults to <see langword="true"/>.
    /// </summary>
    bool IsSortable { get; }

    /// <summary>
    /// Gets the <see cref="PropertyInfo"/> for default sorting property.
    /// </summary>
    PropertyInfo? DefaultSortingPropertyInfo { get; }
}
