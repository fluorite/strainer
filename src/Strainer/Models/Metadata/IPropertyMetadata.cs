﻿using Fluorite.Strainer.Models.Sorting;
using System.Reflection;

namespace Fluorite.Strainer.Models.Metadata;

/// <summary>
/// Provides metadata for a property.
/// </summary>
public interface IPropertyMetadata
{
    /// <summary>
    /// Gets the display name of the property.
    /// </summary>
    string? DisplayName { get; }

    /// <summary>
    /// Gets a value indicating whether related
    /// property should be used as a default (fallback) property when
    /// no sorting information was provided but sorting was still requested.
    /// <para/>
    /// Default sorting is not perfomed when sorting information is not
    /// properly recognized.
    /// </summary>
    bool IsDefaultSorting { get; }

    /// <summary>
    /// Gets default sorting way used when related property is marked
    /// as a default sorting property.
    /// </summary>
    SortingWay? DefaultSortingWay { get; }

    /// <summary>
    /// Gets a value indicating whether related
    /// property is marked as filterable.
    /// </summary>
    bool IsFilterable { get; }

    /// <summary>
    /// Gets a value indicating whether related
    /// property is marked as sortable.
    /// </summary>
    bool IsSortable { get; }

    /// <summary>
    /// Gets the name of related property.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets the <see cref="System.Reflection.PropertyInfo"/> for related
    /// property.
    /// </summary>
    PropertyInfo? PropertyInfo { get; }
}
