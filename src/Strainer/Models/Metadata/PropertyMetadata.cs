﻿using Fluorite.Strainer.Models.Sorting;
using System.Diagnostics;
using System.Reflection;

namespace Fluorite.Strainer.Models.Metadata;

/// <summary>
/// Represents default property metadata model.
/// </summary>
[DebuggerDisplay("\\{" + nameof(Name) + " = " + "{" + nameof(Name) + "} \\}")]
public class PropertyMetadata : IPropertyMetadata
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PropertyMetadata"/> class.
    /// </summary>
    /// <param name="name">
    /// The property name.
    /// </param>
    /// <param name="propertyInfo">
    /// The property info instance for the property.
    /// </param>
    public PropertyMetadata(string name, PropertyInfo propertyInfo)
    {
        Name = Guard.Against.NullOrWhiteSpace(name);
        PropertyInfo = Guard.Against.Null(propertyInfo);
    }

    /// <summary>
    /// Gets or sets the display name of the property.
    /// </summary>
    public string? DisplayName { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether related
    /// property should be used as a default (fallback) property when
    /// no sorting information was provided but sorting was still requested.
    /// <para/>
    /// Default sorting is not perfomed when sorting information is not
    /// properly recognized.
    /// </summary>
    public bool IsDefaultSorting { get; set; }

    /// <summary>
    /// Gets or sets default sorting way used when related property is marked
    /// as a default sorting property.
    /// </summary>
    public SortingWay? DefaultSortingWay { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether related
    /// property is marked as filterable for Strainer.
    /// </summary>
    public bool IsFilterable { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether related
    /// property is marked as sortable for Strainer.
    /// </summary>
    public bool IsSortable { get; set; }

    /// <inheritdoc/>
    public string Name { get; }

    /// <inheritdoc/>
    public PropertyInfo PropertyInfo { get; }
}
