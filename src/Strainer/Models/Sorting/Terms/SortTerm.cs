﻿using System.Diagnostics;

namespace Fluorite.Strainer.Models.Sorting.Terms;

/// <summary>
/// Provides detailed information about sorting expression.
/// </summary>
[DebuggerDisplay("\\{" +
        nameof(Name) + " = " + "{" + nameof(Name) + "}, " +
        nameof(IsDescending) + " = " + "{" + nameof(IsDescending) + "}" + "\\}")]
public class SortTerm : ISortTerm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SortTerm"/> class.
    /// </summary>
    /// <param name="name">
    /// The name within sorting term.
    /// </param>
    public SortTerm(string name)
    {
        Name = Guard.Against.NullOrWhiteSpace(name);
    }

    /// <summary>
    /// Gets or sets the original input, based on which current sort term was created.
    /// </summary>
    public string? Input { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether current sorting
    /// direction is descending.
    /// </summary>
    public bool IsDescending { get; set; }

    /// <summary>
    /// Gets the name within sorting term.
    /// </summary>
    public string Name { get; }
}
