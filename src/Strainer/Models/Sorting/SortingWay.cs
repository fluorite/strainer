﻿namespace Fluorite.Strainer.Models.Sorting;

/// <summary>
/// Represents sorting way.
/// </summary>
public enum SortingWay
{
    /// <summary>
    /// Ascending sorting way.
    /// </summary>
    Ascending = 1,

    /// <summary>
    /// Descending sorting way.
    /// </summary>
    Descending,
}
