﻿using System.Linq.Expressions;

namespace Fluorite.Strainer.Models.Filtering.Operators;

/// <summary>
/// Represents default information context for filter expression.
/// </summary>
public class FilterExpressionContext : IFilterExpressionContext
{
    /// <summary>
    /// Initializes a new instance of the <see cref="FilterExpressionContext"/>
    /// class.
    /// </summary>
    /// <param name="filterValue">
    /// The filter value expression.
    /// </param>
    /// <param name="propertyValue">
    /// The property access expression.
    /// </param>
    /// <param name="isCaseInsensitiveForValues">
    /// Gets a value indicating whether Strainer should operate in case insensitive mode
    /// when comparing values.
    /// </param>
    /// <param name="isMaterializedQueryable">
    /// A value indicating whether associated <see cref="IQueryable"/>
    /// is in fact a already materialized collection.
    /// </param>
    /// <param name="isStringBasedProperty">
    /// Gets a value indicating whether targeted property is string based.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="filterValue"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="propertyValue"/> is <see langword="null"/>.
    /// </exception>
    public FilterExpressionContext(
        Expression filterValue,
        Expression propertyValue,
        bool isCaseInsensitiveForValues,
        bool isMaterializedQueryable,
        bool isStringBasedProperty)
    {
        FilterValue = Guard.Against.Null(filterValue);
        PropertyValue = Guard.Against.Null(propertyValue);
        IsCaseInsensitiveForValues = isCaseInsensitiveForValues;
        IsMaterializedQueryable = isMaterializedQueryable;
        IsStringBasedProperty = isStringBasedProperty;
    }

    /// <inheritdoc/>
    public Expression FilterValue { get; }

    /// <inheritdoc/>
    public bool IsCaseInsensitiveForValues { get; }

    /// <inheritdoc/>
    public bool IsMaterializedQueryable { get; }

    /// <inheritdoc/>
    public bool IsStringBasedProperty { get; }

    /// <inheritdoc/>
    public Expression PropertyValue { get; }
}
