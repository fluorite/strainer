﻿using System.Linq.Expressions;

namespace Fluorite.Strainer.Models.Filtering.Operators;

/// <summary>
/// Represents information context for filter expression.
/// </summary>
public interface IFilterExpressionContext
{
    /// <summary>
    /// Gets the expression for filter value being processed.
    /// </summary>
    Expression FilterValue { get; }

    /// <summary>
    /// Gets a value indicating whether Strainer should operate in case insensitive mode
    /// when comparing values.
    /// <para/>
    /// Value taken from global <see cref="StrainerOptions.IsCaseInsensitiveForValues"/>.
    /// </summary>
    public bool IsCaseInsensitiveForValues { get; }

    /// <summary>
    /// Gets a value indicating whether associated <see cref="IQueryable"/>
    /// is in fact an already materialized collection. In such case database
    /// collation will not work and other <see cref="IQueryable"/> provider-specific
    /// features, such as EF functions.
    /// </summary>
    bool IsMaterializedQueryable { get; }

    /// <summary>
    /// Gets a value indicating whether targeted property is string based.
    /// </summary>
    bool IsStringBasedProperty { get; }

    /// <summary>
    /// Gets the expression for property value access.
    /// </summary>
    Expression PropertyValue { get; }
}
