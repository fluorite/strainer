﻿using Fluorite.Strainer.Models.Filtering.Operators;

namespace Fluorite.Strainer.Models.Filtering.Terms;

/// <summary>
/// Holds segregated details about a single filtering term.
/// </summary>
public class FilterTerm : IFilterTerm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="FilterTerm"/> class.
    /// </summary>
    /// <param name="input">
    /// The filter input from Strainer model.
    /// </param>
    public FilterTerm(string input)
    {
        Input = input;
        Names = new List<string>();
        Values = new List<string>();
    }

    /// <inheritdoc/>
    public string Input { get; }

    /// <summary>
    /// Gets or sets the list of names.
    /// </summary>
    public IList<string> Names { get; set; }

    /// <summary>
    /// Gets or sets the filter operator.
    /// </summary>
    public IFilterOperator? Operator { get; set; }

    /// <summary>
    /// Gets or sets the list of values.
    /// </summary>
    public IList<string> Values { get; set; }
}
