﻿namespace Fluorite.Strainer.Exceptions;

public abstract class StrainerNotFoundException : StrainerException
{
    protected StrainerNotFoundException(string name)
    {
        Name = Guard.Against.Null(name);
    }

    protected StrainerNotFoundException(string name, string message) : base(message)
    {
        Name = Guard.Against.Null(name);
    }

    protected StrainerNotFoundException(string name, string message, Exception innerException) : base(message, innerException)
    {
        Name = Guard.Against.Null(name);
    }

    public string Name { get; }
}
