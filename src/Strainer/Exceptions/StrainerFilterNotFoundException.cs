﻿namespace Fluorite.Strainer.Exceptions;

public class StrainerFilterNotFoundException : StrainerNotFoundException
{
    public StrainerFilterNotFoundException(string name) : base(name)
    {
    }

    public StrainerFilterNotFoundException(string name, string message) : base(name, message)
    {
    }

    public StrainerFilterNotFoundException(string name, string message, Exception innerException) : base(name, message, innerException)
    {
    }
}
