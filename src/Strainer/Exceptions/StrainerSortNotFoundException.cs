﻿namespace Fluorite.Strainer.Exceptions;

public class StrainerSortNotFoundException : StrainerNotFoundException
{
    public StrainerSortNotFoundException(string name) : base(name)
    {
    }

    public StrainerSortNotFoundException(string name, string message) : base(name, message)
    {
    }

    public StrainerSortNotFoundException(string name, string message, Exception innerException) : base(name, message, innerException)
    {
    }
}
