﻿using Fluorite.Strainer.Models.Filtering.Operators;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public class FilterOperatorBuilderWithName : FilterOperatorBuilderWithSymbol, IFilterOperatorBuilderWithName
{
    public FilterOperatorBuilderWithName(string symbol, string name)
        : base(symbol)
    {
        Name = Guard.Against.NullOrWhiteSpace(name);
    }

    protected string Name { get; }

    public IFilterOperatorBuilderWithExpression HasExpression(Func<IFilterExpressionContext, Expression> expression)
    {
        Guard.Against.Null(expression);

        return new FilterOperatorBuilderWithExpression(Symbol, Name, expression);
    }
}
