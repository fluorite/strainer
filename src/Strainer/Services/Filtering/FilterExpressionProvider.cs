﻿using Fluorite.Strainer.Models.Filtering.Terms;
using Fluorite.Strainer.Models.Metadata;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public class FilterExpressionProvider : IFilterExpressionProvider
{
    private readonly IFilterExpressionWorkflowBuilder _filterExpressionWorkflowBuilder;

    public FilterExpressionProvider(
        IFilterExpressionWorkflowBuilder filterExpressionWorkflowBuilder)
    {
        _filterExpressionWorkflowBuilder = Guard.Against.Null(filterExpressionWorkflowBuilder);
    }

    public Expression? GetExpression(
        IPropertyMetadata metadata,
        IFilterTerm filterTerm,
        ParameterExpression parameterExpression,
        Expression? innerExpression,
        bool isMaterializedQueryable)
    {
        Guard.Against.Null(metadata);
        Guard.Against.Null(filterTerm);
        Guard.Against.Null(parameterExpression);

        if (filterTerm.Values == null || !filterTerm.Values.Any())
        {
            return null;
        }

        var nameParts = metadata.Name.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
        if (!nameParts.Any())
        {
            throw new ArgumentException("Metadata name must not be empty.", nameof(metadata));
        }

        MemberExpression? propertyExpresssion = null;

        foreach (var part in nameParts)
        {
            propertyExpresssion = Expression.PropertyOrField((Expression?)propertyExpresssion ?? parameterExpression, part);
        }

        return CreateInnerExpression(
            metadata,
            filterTerm,
            innerExpression,
            propertyExpresssion,
            isMaterializedQueryable);
    }

    private Expression? CreateInnerExpression(
        IPropertyMetadata metadata,
        IFilterTerm filterTerm,
        Expression? innerExpression,
        MemberExpression? propertyExpression,
        bool isMaterializedQueryable)
    {
        var workflow = _filterExpressionWorkflowBuilder.BuildDefaultWorkflow();

        foreach (var filterTermValue in filterTerm.Values)
        {
            var context = new FilterExpressionWorkflowContext
            {
                FilterTermConstant = filterTermValue,
                FilterTermValue = filterTermValue,
                FinalExpression = null,
                IsMaterializedQueryable = isMaterializedQueryable,
                PropertyMetadata = metadata,
                PropertyValue = propertyExpression,
                Term = filterTerm,
            };

            var expression = workflow.Run(context);

            if (innerExpression == null)
            {
                innerExpression = expression;
            }
            else
            {
                innerExpression = Expression.Or(innerExpression, expression);
            }
        }

        return innerExpression;
    }
}
