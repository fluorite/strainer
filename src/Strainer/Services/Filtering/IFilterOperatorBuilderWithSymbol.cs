﻿namespace Fluorite.Strainer.Services.Filtering;

public interface IFilterOperatorBuilderWithSymbol : IFilterOperatorBuilder
{
    IFilterOperatorBuilderWithName HasName(string name);
}
