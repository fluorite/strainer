﻿using Fluorite.Extensions;
using Fluorite.Strainer.Models.Filtering.Operators;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Reflection;

namespace Fluorite.Strainer.Services.Filtering;

public static class FilterOperatorMapper
{
    static FilterOperatorMapper()
    {
        DefaultOperators = new ReadOnlyDictionary<string, IFilterOperator>(
            GetDefaultFilterOperators()
                .ToDictionary(filterOperator => filterOperator.Symbol, filterOperator => filterOperator));
    }

    public static IReadOnlyDictionary<string, IFilterOperator> DefaultOperators { get; }

    private static IFilterOperator[] GetDefaultFilterOperators()
    {
        return GetEqualFilterOperators()
            .Concat(
                GetLessThanFilterOperators(),
                GetGreaterThanFilterOperators(),
                GetStringFilterOperators(),
                GetStringNegatedFilterOperators(),
                GetEqualCaseInsensitiveFilterOperators(),
                GetStringCaseInsensitiveFilterOperators(),
                GetStringNegatedCaseInsensitiveFilterOperators())
            .ToArray();
    }

    private static List<IFilterOperator> GetEqualFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.EqualsSymbol)
                .HasName("equal")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues && context.IsStringBasedProperty)
                    {
                        return Expression.Call(
                            typeof(string).GetMethod(
                                nameof(string.Equals),
                                BindingFlags.Static | BindingFlags.Public,
                                null,
                                new Type[] { typeof(string), typeof(string), typeof(StringComparison) },
                                null),
                            context.PropertyValue,
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Equal(context.PropertyValue, context.FilterValue);
                    }
                })
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotEqual)
                .HasName("does not equal")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues && context.IsStringBasedProperty)
                    {
                        return Expression.Not(Expression.Call(
                            typeof(string).GetMethod(
                                nameof(string.Equals),
                                BindingFlags.Static | BindingFlags.Public,
                                null,
                                new Type[] { typeof(string), typeof(string), typeof(StringComparison) },
                                null),
                            context.PropertyValue,
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.NotEqual(context.PropertyValue, context.FilterValue);
                    }
                })
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetLessThanFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder().HasSymbol(FilterOperatorSymbols.LessThan)
                .HasName("less than")
                .HasExpression((context) => Expression.LessThan(context.PropertyValue, context.FilterValue))
                .Build(),
            new FilterOperatorBuilder().HasSymbol(FilterOperatorSymbols.LessThanOrEqualTo)
                .HasName("less than or equal to")
                .HasExpression((context) => Expression.LessThanOrEqual(context.PropertyValue, context.FilterValue))
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetGreaterThanFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder().HasSymbol(FilterOperatorSymbols.GreaterThan)
                .HasName("greater than")
                .HasExpression((context) => Expression.GreaterThan(context.PropertyValue, context.FilterValue))
                .Build(),
            new FilterOperatorBuilder().HasSymbol(FilterOperatorSymbols.GreaterThanOrEqualTo)
                .HasName("greater than or equal to")
                .HasExpression((context) => Expression.GreaterThanOrEqual(context.PropertyValue, context.FilterValue))
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetStringFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.Contains)
                .HasName("contains")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues)
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string) }),
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.StartsWith)
                .HasName("starts with")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues)
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string) }),
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.EndsWith)
                .HasName("ends with")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues)
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string) }),
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetStringNegatedFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotContain)
                .HasName("does not contain")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues)
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string) }),
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotStartWith)
                .HasName("does not start with")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues)
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string) }),
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotEndWith)
                .HasName("does not end with")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable && context.IsCaseInsensitiveForValues)
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string) }),
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetEqualCaseInsensitiveFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.EqualsCaseInsensitive)
                .HasName("equal (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Call(
                            typeof(string).GetMethod(
                                nameof(string.Equals),
                                BindingFlags.Static | BindingFlags.Public,
                                null,
                                new Type[] { typeof(string), typeof(string), typeof(StringComparison) },
                                null),
                            context.PropertyValue,
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            typeof(string).GetMethod(
                                nameof(string.Equals),
                                BindingFlags.Static | BindingFlags.Public,
                                null,
                                new Type[] { typeof(string), typeof(string) },
                                null),
                            context.PropertyValue,
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotEqualCaseInsensitive)
                .HasName("does not equal (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Not(Expression.Call(
                            typeof(string).GetMethod(
                                nameof(string.Equals),
                                BindingFlags.Static | BindingFlags.Public,
                                null,
                                new Type[] { typeof(string), typeof(string), typeof(StringComparison) },
                                null),
                            context.PropertyValue,
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            typeof(string).GetMethod(
                                nameof(string.Equals),
                                BindingFlags.Static | BindingFlags.Public,
                                null,
                                new Type[] { typeof(string), typeof(string) },
                                null),
                            context.PropertyValue,
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetStringCaseInsensitiveFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.ContainsCaseInsensitive)
                .HasName("contains (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string) }),
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.StartsWithCaseInsensitive)
                .HasName("starts with (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string) }),
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.EndsWithCaseInsensitive)
                .HasName("ends with (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        return Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string) }),
                            context.FilterValue);
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
        };
    }

    private static IEnumerable<IFilterOperator> GetStringNegatedCaseInsensitiveFilterOperators()
    {
        return new List<IFilterOperator>
        {
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotContainCaseInsensitive)
                .HasName("does not contain (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.Contains), new Type[] { typeof(string) }),
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotStartWithCaseInsensitive)
                .HasName("does not start with (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.StartsWith), new Type[] { typeof(string) }),
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
            new FilterOperatorBuilder()
                .HasSymbol(FilterOperatorSymbols.DoesNotEndWithCaseInsensitive)
                .HasName("does not end with (case insensitive)")
                .HasExpression((context) =>
                {
                    if (context.IsMaterializedQueryable)
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string), typeof(StringComparison) }),
                            context.FilterValue,
                            Expression.Constant(StringComparison.OrdinalIgnoreCase)));
                    }
                    else
                    {
                        return Expression.Not(Expression.Call(
                            context.PropertyValue,
                            typeof(string).GetMethod(nameof(string.EndsWith), new Type[] { typeof(string) }),
                            context.FilterValue));
                    }
                })
                .IsStringBased()
                .IsCaseInsensitive()
                .Build(),
        };
    }
}
