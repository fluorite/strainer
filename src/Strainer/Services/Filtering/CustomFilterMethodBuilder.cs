﻿namespace Fluorite.Strainer.Services.Filtering;

public class CustomFilterMethodBuilder<TEntity> : ICustomFilterMethodBuilder<TEntity>
{
    public CustomFilterMethodBuilder()
    {
    }

    public ICustomFilterMethodBuilderWithName<TEntity> HasName(string name)
    {
        return new CustomFilterMethodBuilderWithName<TEntity>(name);
    }
}
