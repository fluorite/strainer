﻿namespace Fluorite.Strainer.Services.Filtering;

public class FilterOperatorBuilderWithSymbol : FilterOperatorBuilder, IFilterOperatorBuilderWithSymbol
{
    public FilterOperatorBuilderWithSymbol(string symbol)
    {
        Symbol = Guard.Against.NullOrWhiteSpace(symbol);
    }

    protected string Symbol { get; }

    public IFilterOperatorBuilderWithName HasName(string name)
    {
        Guard.Against.NullOrWhiteSpace(name);

        return new FilterOperatorBuilderWithName(Symbol, name);
    }
}
