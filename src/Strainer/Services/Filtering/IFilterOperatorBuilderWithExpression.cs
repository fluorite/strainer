﻿using Fluorite.Strainer.Models.Filtering.Operators;

namespace Fluorite.Strainer.Services.Filtering;

public interface IFilterOperatorBuilderWithExpression : IFilterOperatorBuilderWithName
{
    IFilterOperator Build();

    IFilterOperatorBuilderWithExpression IsCaseInsensitive();

    IFilterOperatorBuilderWithExpression IsStringBased();
}
