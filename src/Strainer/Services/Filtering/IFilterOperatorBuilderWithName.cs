﻿using Fluorite.Strainer.Models.Filtering.Operators;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public interface IFilterOperatorBuilderWithName : IFilterOperatorBuilderWithSymbol
{
    IFilterOperatorBuilderWithExpression HasExpression(Func<IFilterExpressionContext, Expression> expression);
}
