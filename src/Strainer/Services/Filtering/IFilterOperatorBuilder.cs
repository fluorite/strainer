﻿namespace Fluorite.Strainer.Services.Filtering;

public interface IFilterOperatorBuilder
{
    IFilterOperatorBuilderWithSymbol HasSymbol(string symbol);
}
