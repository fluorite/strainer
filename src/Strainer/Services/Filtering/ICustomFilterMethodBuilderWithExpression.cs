﻿using Fluorite.Strainer.Models.Filtering;

namespace Fluorite.Strainer.Services.Filtering;

public interface ICustomFilterMethodBuilderWithExpression<TEntity> : ICustomFilterMethodBuilderWithName<TEntity>
{
    ICustomFilterMethod<TEntity> Build();
}
