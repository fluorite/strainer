﻿using Fluorite.Strainer.Models.Filtering.Operators;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public class FilterOperatorBuilderWithExpression : FilterOperatorBuilderWithName, IFilterOperatorBuilderWithExpression
{
    public FilterOperatorBuilderWithExpression(
        string symbol,
        string name,
        Func<IFilterExpressionContext, Expression> expression)
        : base(symbol, name)
    {
        Expression = Guard.Against.Null(expression);
    }

    protected Func<IFilterExpressionContext, Expression> Expression { get; }

    protected bool IsCaseInsensitiveValue { get; private set; }

    protected bool IsStringBasedValue { get; private set; }

    public IFilterOperator Build()
    {
        return new FilterOperator(Name, Symbol, Expression)
        {
            IsCaseInsensitive = IsCaseInsensitiveValue,
            IsStringBased = IsStringBasedValue,
        };
    }

    public IFilterOperatorBuilderWithExpression IsCaseInsensitive()
    {
        IsCaseInsensitiveValue = true;

        return this;
    }

    public IFilterOperatorBuilderWithExpression IsStringBased()
    {
        IsStringBasedValue = true;

        return this;
    }
}
