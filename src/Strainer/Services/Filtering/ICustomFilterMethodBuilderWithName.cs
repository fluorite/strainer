﻿using Fluorite.Strainer.Models.Filtering.Terms;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public interface ICustomFilterMethodBuilderWithName<TEntity> : ICustomFilterMethodBuilder<TEntity>
{
    ICustomFilterMethodBuilderWithExpression<TEntity> HasFunction(Expression<Func<TEntity, bool>> expression);

    ICustomFilterMethodBuilderWithExpression<TEntity> HasFunction(Func<IFilterTerm, Expression<Func<TEntity, bool>>> filterTermExpression);
}
