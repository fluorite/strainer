﻿namespace Fluorite.Strainer.Services.Filtering;

public interface ICustomFilterMethodBuilder<TEntity>
{
    ICustomFilterMethodBuilderWithName<TEntity> HasName(string name);
}
