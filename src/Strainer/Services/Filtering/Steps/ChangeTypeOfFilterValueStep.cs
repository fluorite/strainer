﻿using Fluorite.Strainer.Services.Conversion;

namespace Fluorite.Strainer.Services.Filtering.Steps;

public class ChangeTypeOfFilterValueStep : IChangeTypeOfFilterValueStep
{
    private readonly ITypeChanger _typeChanger;
    private readonly ITypeConverterProvider _typeConverterProvider;

    public ChangeTypeOfFilterValueStep(
        ITypeChanger typeChanger,
        ITypeConverterProvider typeConverterProvider)
    {
        _typeChanger = Guard.Against.Null(typeChanger);
        _typeConverterProvider = Guard.Against.Null(typeConverterProvider);
    }

    public void Execute(FilterExpressionWorkflowContext context)
    {
        Guard.Against.Null(context);
        Guard.Against.Null(context.Term);
        Guard.Against.Null(context.Term.Operator);
        Guard.Against.Null(context.PropertyMetadata);
        Guard.Against.Null(context.PropertyMetadata.PropertyInfo);
        Guard.Against.Null(context.FilterTermValue);

        if (context.Term.Operator.IsStringBased)
        {
            return;
        }

        var propertyType = context.PropertyMetadata!.PropertyInfo!.PropertyType;
        var typeConverter = _typeConverterProvider.GetTypeConverter(propertyType);
        var canConvertFromString =
               context.PropertyMetadata.PropertyInfo.PropertyType != typeof(string)
            && typeConverter.CanConvertFrom(typeof(string));

        if (canConvertFromString == false)
        {
            context.FilterTermConstant = _typeChanger.ChangeType(
                context.FilterTermValue,
                context.PropertyMetadata.PropertyInfo.PropertyType);
        }
    }
}
