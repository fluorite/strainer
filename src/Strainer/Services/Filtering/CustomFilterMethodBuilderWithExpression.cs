﻿using Fluorite.Strainer.Models.Filtering;
using Fluorite.Strainer.Models.Filtering.Terms;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public class CustomFilterMethodBuilderWithExpression<TEntity> : CustomFilterMethodBuilderWithName<TEntity>, ICustomFilterMethodBuilderWithExpression<TEntity>
{
    public CustomFilterMethodBuilderWithExpression(
        string name,
        Expression<Func<TEntity, bool>> expression)
        : base(name)
    {
        Expression = Guard.Against.Null(expression);
    }

    public CustomFilterMethodBuilderWithExpression(
        string name,
        Func<IFilterTerm, Expression<Func<TEntity, bool>>> filterTermExpression)
        : base(name)
    {
        FilterTermExpression = Guard.Against.Null(filterTermExpression);
    }

    public Expression<Func<TEntity, bool>>? Expression { get; }

    public Func<IFilterTerm, Expression<Func<TEntity, bool>>>? FilterTermExpression { get; }

    public ICustomFilterMethod<TEntity> Build()
    {
        if (FilterTermExpression is null)
        {
            return new CustomFilterMethod<TEntity>(Name, Expression!);
        }
        else
        {
            return new CustomFilterMethod<TEntity>(Name, FilterTermExpression);
        }
    }
}
