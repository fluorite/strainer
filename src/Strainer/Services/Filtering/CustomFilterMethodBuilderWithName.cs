﻿using Fluorite.Strainer.Models.Filtering.Terms;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Filtering;

public class CustomFilterMethodBuilderWithName<TEntity> : CustomFilterMethodBuilder<TEntity>, ICustomFilterMethodBuilderWithName<TEntity>
{
    public CustomFilterMethodBuilderWithName(string name)
    {
        Name = Guard.Against.NullOrWhiteSpace(name);
    }

    protected string Name { get; }

    public ICustomFilterMethodBuilderWithExpression<TEntity> HasFunction(Expression<Func<TEntity, bool>> expression)
    {
        Guard.Against.Null(expression);

        return new CustomFilterMethodBuilderWithExpression<TEntity>(Name, expression);
    }

    public ICustomFilterMethodBuilderWithExpression<TEntity> HasFunction(
        Func<IFilterTerm, Expression<Func<TEntity, bool>>> filterTermExpression)
    {
        Guard.Against.Null(filterTermExpression);

        return new CustomFilterMethodBuilderWithExpression<TEntity>(Name, filterTermExpression);
    }
}
