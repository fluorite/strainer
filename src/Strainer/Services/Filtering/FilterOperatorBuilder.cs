﻿namespace Fluorite.Strainer.Services.Filtering;

public class FilterOperatorBuilder : IFilterOperatorBuilder
{
    public IFilterOperatorBuilderWithSymbol HasSymbol(string symbol)
    {
        Guard.Against.NullOrWhiteSpace(symbol);

        return new FilterOperatorBuilderWithSymbol(symbol);
    }
}
