﻿namespace Fluorite.Strainer.Services.Sorting;

public class CustomSortMethodBuilder<TEntity> : ICustomSortMethodBuilder<TEntity>
{
    public CustomSortMethodBuilder()
    {
    }

    public ICustomSortMethodBuilderWithName<TEntity> HasName(string name)
    {
        Guard.Against.NullOrWhiteSpace(name);

        return new CustomSortMethodBuilderWithName<TEntity>(name);
    }
}