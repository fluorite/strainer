﻿using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Services.Metadata;
using System.Reflection;

namespace Fluorite.Strainer.Services.Sorting;

public class SortPropertyMetadataBuilder<TEntity> : PropertyMetadataBuilder<TEntity>, ISortPropertyMetadataBuilder<TEntity>
{
    public SortPropertyMetadataBuilder(
        IDictionary<Type, IDictionary<string, IPropertyMetadata>> propertyMetadata,
        IDictionary<Type, IPropertyMetadata> defaultMetadata,
        PropertyInfo propertyInfo,
        string fullName,
        IPropertyMetadata basePropertyMetadata)
        : base(propertyMetadata, defaultMetadata, propertyInfo, fullName)
    {
        Guard.Against.Null(basePropertyMetadata);

        DisplayName = basePropertyMetadata.DisplayName;
        IsDefaultSorting = basePropertyMetadata.IsDefaultSorting;
        DefaultSortingWay = basePropertyMetadata.DefaultSortingWay;
        IsFilterableValue = basePropertyMetadata.IsFilterable;
        IsSortableValue = basePropertyMetadata.IsSortable;

        Save(Build());
    }

    public ISortPropertyMetadataBuilder<TEntity> IsDefaultSort(bool isDescending = false)
    {
        IsDefaultSorting = true;
        DefaultSortingWay = isDescending ? SortingWay.Descending : SortingWay.Ascending;
        Save(Build());

        return this;
    }
}
