﻿using Fluorite.Strainer.Models.Sorting;

namespace Fluorite.Strainer.Services.Sorting;

public interface ICustomSortMethodBuilderWithExpression<TEntity> : ICustomSortMethodBuilderWithName<TEntity>
{
    ICustomSortMethod<TEntity> Build();
}
