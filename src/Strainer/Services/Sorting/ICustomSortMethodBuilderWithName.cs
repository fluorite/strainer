﻿using Fluorite.Strainer.Models.Sorting.Terms;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Sorting;

public interface ICustomSortMethodBuilderWithName<TEntity> : ICustomSortMethodBuilder<TEntity>
{
    ICustomSortMethodBuilderWithExpression<TEntity> HasFunction(Expression<Func<TEntity, object>> expression);

    ICustomSortMethodBuilderWithExpression<TEntity> HasFunction(Func<ISortTerm, Expression<Func<TEntity, object>>> sortTermExpression);
}
