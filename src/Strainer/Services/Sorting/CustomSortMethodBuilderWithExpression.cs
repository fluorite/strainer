﻿using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Models.Sorting.Terms;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Sorting;

public class CustomSortMethodBuilderWithExpression<TEntity> : CustomSortMethodBuilderWithName<TEntity>, ICustomSortMethodBuilderWithExpression<TEntity>
{
    public CustomSortMethodBuilderWithExpression(
        string name, Expression<Func<TEntity, object>> expression)
        : base(name)
    {
        Expression = Guard.Against.Null(expression);
    }

    public CustomSortMethodBuilderWithExpression(
        string name,
        Func<ISortTerm, Expression<Func<TEntity, object>>> sortTermExpression)
        : base(name)
    {
        SortTermExpression = Guard.Against.Null(sortTermExpression);
    }

    public Expression<Func<TEntity, object>>? Expression { get; }

    public Func<ISortTerm, Expression<Func<TEntity, object>>>? SortTermExpression { get; }

    public ICustomSortMethod<TEntity> Build()
    {
        if (SortTermExpression is null)
        {
            return new CustomSortMethod<TEntity>(Name, Expression!);
        }
        else
        {
            return new CustomSortMethod<TEntity>(Name, SortTermExpression);
        }
    }
}
