﻿namespace Fluorite.Strainer.Services.Sorting;

public interface ICustomSortMethodBuilder<TEntity>
{
    ICustomSortMethodBuilderWithName<TEntity> HasName(string name);
}
