﻿using Fluorite.Strainer.Models.Sorting.Terms;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Sorting;

public class CustomSortMethodBuilderWithName<TEntity> : CustomSortMethodBuilder<TEntity>, ICustomSortMethodBuilderWithName<TEntity>
{
    public CustomSortMethodBuilderWithName(string name)
    {
        Name = Guard.Against.NullOrWhiteSpace(name);
    }

    protected string Name { get; }

    public ICustomSortMethodBuilderWithExpression<TEntity> HasFunction(Expression<Func<TEntity, object>> expression)
    {
        Guard.Against.Null(expression);

        return new CustomSortMethodBuilderWithExpression<TEntity>(Name, expression);
    }

    public ICustomSortMethodBuilderWithExpression<TEntity> HasFunction(
        Func<ISortTerm, Expression<Func<TEntity, object>>> sortTermExpression)
    {
        Guard.Against.Null(sortTermExpression);

        return new CustomSortMethodBuilderWithExpression<TEntity>(Name, sortTermExpression);
    }
}
