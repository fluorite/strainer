﻿using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using Fluorite.Strainer.Models.Sorting.Terms;
using Fluorite.Strainer.Services.Metadata;
using System.Linq.Expressions;

namespace Fluorite.Strainer.Services.Sorting;

/// <summary>
/// Provides means of tranlating <see cref="ISortTerm"/> into
/// <see cref="Expression{TDelegate}"/> of <see cref="Func{T, TResult}"/>.
/// <para/>
/// In other words - provides list of expressions which later can be used
/// as arguments for ordering <see cref="IQueryable{T}"/>.
/// </summary>
public class SortExpressionProvider : ISortExpressionProvider
{
    private readonly IMetadataFacade _metadataProvidersFacade;
    private readonly IStrainerOptionsProvider _strainerOptionsProvider;

    /// <summary>
    /// Initializes a new instance of the <see cref="SortExpressionProvider"/> class.
    /// </summary>
    public SortExpressionProvider(
        IMetadataFacade metadataProvidersFacade,
        IStrainerOptionsProvider strainerOptionsProvider)
    {
        _metadataProvidersFacade = Guard.Against.Null(metadataProvidersFacade);
        _strainerOptionsProvider = Guard.Against.Null(strainerOptionsProvider);
    }

    /// <inheritdoc/>
    public ISortExpression<TEntity>? GetDefaultExpression<TEntity>()
    {
        var propertyMetadata = _metadataProvidersFacade.GetDefaultMetadata<TEntity>();
        if (propertyMetadata is null)
        {
            return null;
        }

        var name = propertyMetadata.DisplayName ?? propertyMetadata.Name;
        var defaultSortingWay = propertyMetadata.DefaultSortingWay
            ?? _strainerOptionsProvider.GetStrainerOptions().DefaultSortingWay;
        var isDescending = defaultSortingWay == SortingWay.Descending;
        var sortTerm = new SortTerm(name)
        {
            IsDescending = isDescending,
        };

        return GetExpression<TEntity>(propertyMetadata, sortTerm, isSubsequent: false);
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="propertyMetadata"/> is <see langword="null"/>.
    /// <paramref name="sortTerm"/> is <see langword="null"/>.
    /// </exception>
    public ISortExpression<TEntity> GetExpression<TEntity>(
        IPropertyMetadata propertyMetadata,
        ISortTerm sortTerm,
        bool isSubsequent)
    {
        Guard.Against.Null(propertyMetadata);
        Guard.Against.Null(sortTerm);

        var parameter = Expression.Parameter(typeof(TEntity), "p");
        Expression propertyValue = parameter;

        if (propertyMetadata.Name.Contains("."))
        {
            var parts = propertyMetadata.Name.Split('.');

            for (var i = 0; i < parts.Length - 1; i++)
            {
                propertyValue = Expression.PropertyOrField(propertyValue, parts[i]);
            }
        }

        var propertyAccess = Expression.MakeMemberAccess(propertyValue, propertyMetadata.PropertyInfo!);
        var conversion = Expression.Convert(propertyAccess, typeof(object));
        var orderExpression = Expression.Lambda<Func<TEntity, object>>(conversion, parameter);

        return new SortExpression<TEntity>(orderExpression)
        {
            IsDefault = propertyMetadata.IsDefaultSorting,
            IsDescending = sortTerm.IsDescending,
            IsSubsequent = isSubsequent,
        };
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="sortTerms"/> is <see langword="null"/>.
    /// </exception>
    public IReadOnlyList<ISortExpression<TEntity>> GetExpressions<TEntity>(
        IEnumerable<KeyValuePair<IPropertyMetadata, ISortTerm>> sortTerms)
    {
        Guard.Against.Null(sortTerms);

        var expressions = new List<ISortExpression<TEntity>>();
        var isSubqequent = false;

        foreach (var pair in sortTerms)
        {
            var sortExpression = GetExpression<TEntity>(pair.Key, pair.Value, isSubqequent);
            if (sortExpression != null)
            {
                expressions.Add(sortExpression);
            }
            else
            {
                continue;
            }

            isSubqequent = true;
        }

        return expressions.AsReadOnly();
    }
}
