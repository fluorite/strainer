﻿using Fluorite.Strainer.Models;
using Fluorite.Strainer.Services.Pipelines;

namespace Fluorite.Strainer.Services;

/// <summary>
/// Default implementation of Strainer main service taking care of filtering,
/// sorting and pagination.
/// </summary>
public class StrainerProcessor : IStrainerProcessor
{
    private readonly IStrainerPipelineBuilderFactory _strainerPipelineBuilderFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="StrainerProcessor"/> class
    /// with specified context.
    /// </summary>
    /// <param name="strainerPipelineBuilderFactory"></param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="strainerPipelineBuilderFactory"/> is <see langword="null"/>.
    /// </exception>
    public StrainerProcessor(
        IStrainerPipelineBuilderFactory strainerPipelineBuilderFactory)
    {
        _strainerPipelineBuilderFactory = Guard.Against.Null(strainerPipelineBuilderFactory);
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="model"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="source"/> is <see langword="null"/>.
    /// </exception>
    public IQueryable<TEntity> Apply<TEntity>(
        IStrainerModel model,
        IQueryable<TEntity> source,
        bool applyFiltering = true,
        bool applySorting = true,
        bool applyPagination = true)
    {
        Guard.Against.Null(model);
        Guard.Against.Null(source);

        var builder = _strainerPipelineBuilderFactory.CreateBuilder();

        if (applyFiltering)
        {
            builder.Filter();
        }

        if (applySorting)
        {
            builder.Sort();
        }

        if (applyPagination)
        {
            builder.Paginate();
        }

        var pipeline = builder.Build();

        return pipeline.Run(model, source);
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="model"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="source"/> is <see langword="null"/>.
    /// </exception>
    public IQueryable<TEntity> ApplyFiltering<TEntity>(
        IStrainerModel model,
        IQueryable<TEntity> source)
    {
        Guard.Against.Null(model);
        Guard.Against.Null(source);

        var pipeline = _strainerPipelineBuilderFactory
            .CreateBuilder()
            .Filter()
            .Build();

        return pipeline.Run(model, source);
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="model"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="source"/> is <see langword="null"/>.
    /// </exception>
    public IQueryable<TEntity> ApplyPagination<TEntity>(
        IStrainerModel model,
        IQueryable<TEntity> source)
    {
        Guard.Against.Null(model);
        Guard.Against.Null(source);

        var pipeline = _strainerPipelineBuilderFactory
            .CreateBuilder()
            .Paginate()
            .Build();

        return pipeline.Run(model, source);
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="model"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="source"/> is <see langword="null"/>.
    /// </exception>
    public IQueryable<TEntity> ApplySorting<TEntity>(
        IStrainerModel model,
        IQueryable<TEntity> source)
    {
        Guard.Against.Null(model);
        Guard.Against.Null(source);

        var pipeline = _strainerPipelineBuilderFactory
            .CreateBuilder()
            .Sort()
            .Build();

        return pipeline.Run(model, source);
    }
}
