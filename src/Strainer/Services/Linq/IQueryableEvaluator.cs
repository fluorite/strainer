﻿namespace Fluorite.Strainer.Services.Linq;

public interface IQueryableEvaluator
{
    bool IsMaterialized<T>(IQueryable<T> queryable);
}
