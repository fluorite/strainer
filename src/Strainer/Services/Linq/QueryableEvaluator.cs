﻿using System.Reflection;

namespace Fluorite.Strainer.Services.Linq;

public class QueryableEvaluator : IQueryableEvaluator
{
    public bool IsMaterialized<T>(IQueryable<T> queryable)
    {
        Guard.Against.Null(queryable);

        if (queryable is not EnumerableQuery<T> enumerableQuery)
        {
            return false;
        }

        var type = enumerableQuery.GetType();
        var property = type.GetProperty("Enumerable", BindingFlags.NonPublic | BindingFlags.Instance);
        var propertyValue = property.GetValue(queryable, null);

        return propertyValue is Array
            || propertyValue is ICollection<T>
            || propertyValue is IReadOnlyCollection<T>;
    }
}
