﻿using Fluorite.Strainer.Models.Metadata;
using System.Reflection;

namespace Fluorite.Strainer.Services.Metadata.FluentApi;

public interface IFluentApiPropertyMetadataBuilder
{
    IPropertyMetadata BuildDefaultMetadata(IObjectMetadata objectMetadata);

    IPropertyMetadata BuildMetadataForProperty(IObjectMetadata objectMetadata, PropertyInfo propertyInfo);
}
