﻿using Fluorite.Strainer.Models.Metadata;
using System.Reflection;

namespace Fluorite.Strainer.Services.Metadata.FluentApi;

public class FluentApiPropertyMetadataBuilder : IFluentApiPropertyMetadataBuilder
{
    private readonly IStrainerOptionsProvider _strainerOptionsProvider;

    public FluentApiPropertyMetadataBuilder(IStrainerOptionsProvider strainerOptionsProvider)
    {
        _strainerOptionsProvider = Guard.Against.Null(strainerOptionsProvider);
    }

    public IPropertyMetadata BuildDefaultMetadata(IObjectMetadata objectMetadata)
    {
        Guard.Against.Null(objectMetadata);
        Guard.Against.Null(objectMetadata.DefaultSortingPropertyInfo);

        return new PropertyMetadata(objectMetadata.DefaultSortingPropertyName, objectMetadata.DefaultSortingPropertyInfo)
        {
            IsDefaultSorting = true,
            DefaultSortingWay = objectMetadata.DefaultSortingWay,
            IsFilterable = objectMetadata.IsFilterable,
            IsSortable = objectMetadata.IsSortable,
        };
    }

    public IPropertyMetadata BuildMetadataForProperty(IObjectMetadata objectMetadata, PropertyInfo propertyInfo)
    {
        Guard.Against.Null(objectMetadata);
        Guard.Against.Null(objectMetadata.DefaultSortingPropertyInfo);
        Guard.Against.Null(propertyInfo);

        var isDefaultSorting = objectMetadata.DefaultSortingPropertyInfo == propertyInfo;
        var defaultSortingWay = isDefaultSorting
            ? objectMetadata.DefaultSortingWay
            : _strainerOptionsProvider.GetStrainerOptions().DefaultSortingWay;

        return new PropertyMetadata(propertyInfo.Name, propertyInfo)
        {
            IsFilterable = objectMetadata.IsFilterable,
            IsSortable = objectMetadata.IsSortable,
            IsDefaultSorting = isDefaultSorting,
            DefaultSortingWay = defaultSortingWay,
        };
    }
}
