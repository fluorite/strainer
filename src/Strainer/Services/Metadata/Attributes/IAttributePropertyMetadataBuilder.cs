﻿using Fluorite.Strainer.Attributes;
using Fluorite.Strainer.Models.Metadata;
using System.Reflection;

namespace Fluorite.Strainer.Services.Metadata.Attributes;

public interface IAttributePropertyMetadataBuilder
{
    IPropertyMetadata BuildDefaultMetadata(StrainerObjectAttribute attribute, PropertyInfo propertyInfo);

    IPropertyMetadata BuildDefaultMetadataForProperty(StrainerObjectAttribute attribute, PropertyInfo propertyInfo);
}