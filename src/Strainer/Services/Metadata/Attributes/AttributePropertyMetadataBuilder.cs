﻿using Fluorite.Strainer.Attributes;
using Fluorite.Strainer.Models.Metadata;
using System.Reflection;

namespace Fluorite.Strainer.Services.Metadata.Attributes;

public class AttributePropertyMetadataBuilder : IAttributePropertyMetadataBuilder
{
    private readonly IStrainerOptionsProvider _strainerOptionsProvider;

    public AttributePropertyMetadataBuilder(IStrainerOptionsProvider strainerOptionsProvider)
    {
        _strainerOptionsProvider = Guard.Against.Null(strainerOptionsProvider);
    }

    public IPropertyMetadata BuildDefaultMetadata(StrainerObjectAttribute attribute, PropertyInfo propertyInfo)
    {
        Guard.Against.Null(attribute);
        Guard.Against.Null(propertyInfo);

        return new PropertyMetadata(propertyInfo.Name, propertyInfo)
        {
            IsDefaultSorting = true,
            DefaultSortingWay = attribute.DefaultSortingWay,
            IsFilterable = attribute.IsFilterable,
            IsSortable = attribute.IsSortable,
        };
    }

    public IPropertyMetadata BuildDefaultMetadataForProperty(StrainerObjectAttribute attribute, PropertyInfo propertyInfo)
    {
        Guard.Against.Null(attribute);
        Guard.Against.Null(propertyInfo);

        var isDefaultSorting = attribute.DefaultSortingPropertyInfo == propertyInfo;
        var defaultSortingWay = isDefaultSorting
            ? attribute.DefaultSortingWay
            : _strainerOptionsProvider.GetStrainerOptions().DefaultSortingWay;

        return new PropertyMetadata(propertyInfo.Name, propertyInfo)
        {
            IsDefaultSorting = isDefaultSorting,
            DefaultSortingWay = defaultSortingWay,
            IsFilterable = attribute.IsFilterable,
            IsSortable = attribute.IsSortable,
        };
    }
}
