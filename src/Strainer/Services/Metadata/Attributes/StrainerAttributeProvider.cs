﻿using Fluorite.Strainer.Attributes;
using System.Reflection;

namespace Fluorite.Strainer.Services.Metadata.Attributes;

public class StrainerAttributeProvider : IStrainerAttributeProvider
{
    private readonly IPropertyInfoProvider _propertyInfoProvider;

    public StrainerAttributeProvider(IPropertyInfoProvider propertyInfoProvider)
    {
        _propertyInfoProvider = Guard.Against.Null(propertyInfoProvider);
    }

    public StrainerObjectAttribute? GetObjectAttribute(Type type)
    {
        Guard.Against.Null(type);

        var attribute = type.GetCustomAttribute<StrainerObjectAttribute>(inherit: false);
        if (attribute != null)
        {
            attribute.DefaultSortingPropertyInfo = _propertyInfoProvider.GetPropertyInfo(type, attribute.DefaultSortingPropertyName);
        }

        return attribute;
    }

    public StrainerPropertyAttribute? GetPropertyAttribute(PropertyInfo propertyInfo)
    {
        Guard.Against.Null(propertyInfo);

        var attribute = propertyInfo.GetCustomAttribute<StrainerPropertyAttribute>(inherit: false);
        if (attribute != null)
        {
            attribute.PropertyInfo = propertyInfo;
        }

        return attribute;
    }
}
