﻿using Fluorite.Strainer.Models.Metadata;
using Fluorite.Strainer.Models.Sorting;
using System.Reflection;

namespace Fluorite.Strainer.Attributes;

/// <summary>
/// Marks a class or struct as filterable and/or sortable, setting default
/// values for all its properties.
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
public class StrainerObjectAttribute : Attribute, IObjectMetadata
{
    /// <summary>
    /// Initializes a new instance of the <see cref="StrainerObjectAttribute"/>
    /// class.
    /// </summary>
    /// <param name="defaultSortingPropertyName">
    /// Property name being default sorting property for marked object.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="defaultSortingPropertyName"/> is <see langword="null"/>.
    /// </exception>
    /// <exception cref="ArgumentException">
    /// <paramref name="defaultSortingPropertyName"/> is empty or contains only whitespace characters.
    /// </exception>
    public StrainerObjectAttribute(string defaultSortingPropertyName)
    {
        DefaultSortingPropertyName = Guard.Against.NullOrWhiteSpace(defaultSortingPropertyName);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="StrainerObjectAttribute"/>
    /// class.
    /// </summary>
    /// <param name="defaultSortingPropertyName">
    /// Property name being default sorting property for marked object.
    /// </param>
    /// <param name="defaultSortingWay">
    /// A default sorting way for marked object.
    /// </param>
    /// <exception cref="ArgumentException">
    /// <paramref name="defaultSortingPropertyName"/> is <see langword="null"/>,
    /// empty or contains only whitespace characters.
    /// </exception>
    public StrainerObjectAttribute(string defaultSortingPropertyName, SortingWay defaultSortingWay) : this(defaultSortingPropertyName)
    {
        DefaultSortingWay = defaultSortingWay;
    }

    /// <inheritdoc/>
    public string DefaultSortingPropertyName { get; }

    /// <summary>
    /// Gets default sorting way used when applying sorting using default sorting property.
    /// <para/>
    /// Defaults to <see langword="null"/>.
    /// </summary>
    public SortingWay? DefaultSortingWay { get; }

    /// <summary>
    /// Gets or sets a value indicating whether marked
    /// object is marked as filterable.
    /// <para/>
    /// Defaults to <see langword="true"/>.
    /// </summary>
    public bool IsFilterable { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether marked
    /// object is marked as filterable.
    /// <para/>
    /// Defaults to <see langword="true"/>.
    /// </summary>
    public bool IsSortable { get; set; } = true;

    /// <summary>
    /// Gets or sets the <see cref="PropertyInfo"/> for default sorting property.
    /// </summary>
    public PropertyInfo? DefaultSortingPropertyInfo { get; set; }
}
